within PNRG.Sources;

model ConstantSource
  Real out "Output" annotation(
    Dialog(enable = true, group = "Constant Output"));
  Real cumulativeOutput "Cumulative output of File";
  parameter Integer NOut "Number of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  PNlib.Components.TC t12(arcWeightOut = {out}, maximumSpeed = 1/3600, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {14, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Backend.EnergeticFlowPlace p1(nIn = 1, nOut = NOut) annotation(
    Placement(visible = true, transformation(origin = {68, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.FileOutput fileOutput[NOut] annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  cumulativeOutput = p1.t;
  for i in 1:NOut loop
    connect(p1.outTransition[i], fileOutput[i]) annotation(
      Line(points = {{80, 0}, {110, 0}}));
  end for;
  connect(t12.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{18, 0}, {58, 0}}, thickness = 0.5));
  annotation(
    uses(PNlib(version = "2.2"), Modelica(version = "3.2.3")),
    Diagram,
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Text(origin = {-1, 3}, extent = {{-91, 93}, {91, -93}}, textString = "Const

Input")}));
end ConstantSource;
