within PNRG;

package Sources



  annotation(
    Icon(graphics = {Rectangle(origin = {-18, 0}, fillPattern = FillPattern.Solid, extent = {{-70, 20}, {70, -20}}), Polygon(origin = {62, 0}, fillPattern = FillPattern.Solid, points = {{-30, 60}, {-30, -60}, {30, 0}, {-30, 60}})}));
end Sources;
