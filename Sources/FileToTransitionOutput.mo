within PNRG.Sources;

model FileToTransitionOutput
  Real Output "Output of File";
  Real cumulativeOutput "Cumulative output of File";
  parameter String tableName "Name of table where data is stored" annotation(
    Dialog(enable = true, group = "Data source"));
  parameter String fileName "Name of file where data is stored" annotation(
    Dialog(enable = true, group = "Data source", loadSelector(filter = "Text files (*.txt);;MATLAB MAT-files (*.mat)", caption = "Open file in which table is present")));
  parameter Integer NOut "Number of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  Modelica.Blocks.Tables.CombiTable1D combiTable1D(extrapolation = Modelica.Blocks.Types.Extrapolation.NoExtrapolation, fileName = fileName, smoothness = Modelica.Blocks.Types.Smoothness.LinearSegments, tableName = tableName, tableOnFile = true) annotation(
    Placement(visible = true, transformation(origin = {0, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.RealExpression Idx(y = min(time - floor(time/combiTable1D.u_max)*combiTable1D.u_max, combiTable1D.u_max)) annotation(
    Placement(visible = true, transformation(origin = {-78, 34}, extent = {{-12, -10}, {12, 10}}, rotation = 0)));
  PNlib.Components.TC t12(arcWeightOut = {max(NOut*combiTable1D.y[1], 0)}, maximumSpeed = 1/3600, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {24, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.FileOutput fileOutput[NOut] annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Backend.EnergeticFlowPlace p1(nIn = 1, nOut = NOut)  annotation(
    Placement(visible = true, transformation(origin = {68, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  Output = combiTable1D.y[1];
  cumulativeOutput = p1.t;
  connect(Idx.y, combiTable1D.u[1]) annotation(
    Line(points = {{-65, 34}, {-12, 34}}, color = {0, 0, 127}));
  for i in 1:NOut loop
    connect(p1.outTransition[i], fileOutput[i]) annotation(
      Line(points = {{80, 0}, {110, 0}}));
  end for;
  connect(t12.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{28, 0}, {58, 0}}, thickness = 0.5));
  annotation(
    uses(PNlib(version = "2.2"), Modelica(version = "3.2.3")),
    Diagram,
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Text(origin = {-1, 3}, extent = {{-91, 93}, {91, -93}}, textString = "File
Input")}));
end FileToTransitionOutput;
