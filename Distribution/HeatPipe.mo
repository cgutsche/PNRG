within PNRG.Distribution;

model HeatPipe

parameter Integer NIn "Number of Inputs" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer NOut "Number of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer prioOut[NOut] "Priority of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  Real powerDifference(unit = "kW") "Difference between power suply and consumption";
  Real powerInput(unit = "kW");
  Real powerInputs[NIn];
  Real totalLoad(unit = "kW");
  PNRG.Interfaces.HeatInput heatInput[NIn] annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.HeatOutput heatOutput[NOut] annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Backend.EnergeticTransitionWithoutActivator t1(arcWeightOut = {powerInput}, maximumSpeed = 1/3600, nIn = NIn, nOut = 1)  annotation(
    Placement(visible = true, transformation(origin = {-36, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.PC p1(enablingPrioOut = prioOut, nIn = 1, nOut = NOut) annotation(
    Placement(visible = true, transformation(origin = {46, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  powerInput = sum(powerInputs);
  totalLoad = sum(heatOutput.arcWeight);
  powerDifference = powerInput - totalLoad;
  for i in 1:NIn loop
    powerInputs[i] = heatInput[i].arcWeight*heatInput[i].instSpeed/heatInput[i].maxSpeed;
    connect(heatInput[i], t1.inPlaces[i]) annotation(
      Line(points = {{-31.2, 0}, {34.8, 0}}, thickness = 0.5));
  end for;
  for i in 1:NOut loop
    connect(heatOutput[i], p1.outTransition[i]) annotation(
      Line(points = {{-31.2, 0}, {34.8, 0}}, thickness = 0.5));
  end for;
  connect(t1.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{-32, 0}, {36, 0}}, thickness = 0.5));
  annotation(
    Icon(graphics = {Rectangle(origin = {0, 80}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 20}, {100, -20}}), Text(origin = {-45, 119}, extent = {{-53, 23}, {53, -23}}, textString = "%name"), Ellipse(origin = {-82, 79}, fillColor = {255, 80, 50}, fillPattern = FillPattern.Solid, extent = {{-10, 15}, {10, -15}}), Rectangle(origin = {-2, 79}, fillColor = {255, 80, 50}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-80, 15}, {80, -15}}), Ellipse(origin = {78, 79}, fillColor = {255, 80, 50}, fillPattern = FillPattern.Solid, extent = {{-10, 15}, {10, -15}}), Ellipse(origin = {78, 79}, fillColor = {195, 59, 38}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-8, 13}, {8, -13}}), Line(origin = {-2, 94}, points = {{-80, 0}, {80, 0}, {80, 0}}), Line(origin = {-2, 64}, points = {{80, 0}, {-80, 0}})}, coordinateSystem(extent = {{-120, 40}, {120, -40}})));



end HeatPipe;
