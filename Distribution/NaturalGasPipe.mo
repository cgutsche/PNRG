within PNRG.Distribution;

model NaturalGasPipe
parameter Integer NIn "Number of Inputs" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer NOut "Number of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer prioOut[NOut] "Priority of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  Real massInput(unit = "kg");
  Real totalLoad(unit = "kg");
  PNRG.Interfaces.NaturalGasInput naturalGasInput[NIn] annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.NaturalGasOutput naturalGasOutput[NOut] annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Backend.EnergeticTransitionWithoutActivator t1(arcWeightOut = {massInput}, maximumSpeed = 1/3600, nIn = NIn, nOut = 1)  annotation(
    Placement(visible = true, transformation(origin = {-36, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.PC p1(enablingPrioOut = prioOut, nIn = 1, nOut = NOut) annotation(
    Placement(visible = true, transformation(origin = {46, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  massInput = t1.power*t1.actualSpeed./t1.maximumSpeed;
  totalLoad = sum(naturalGasOutput.arcWeight);
  for i in 1:NIn loop
    connect(naturalGasInput[i], t1.inPlaces[i]) annotation(
      Line(points = {{-31.2, 0}, {34.8, 0}}, thickness = 0.5));
  end for;
  for i in 1:NOut loop
    connect(naturalGasOutput[i], p1.outTransition[i]) annotation(
      Line(points = {{-31.2, 0}, {34.8, 0}}, thickness = 0.5));
  end for;
  connect(t1.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{-32, 0}, {36, 0}}, thickness = 0.5));
  annotation(
    Icon(graphics = {Rectangle(origin = {0, 80}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 20}, {100, -20}}), Text(origin = {-45, 119}, extent = {{-53, 23}, {53, -23}}, textString = "%name"), Ellipse(origin = {-82, 79}, lineColor = {126, 58, 0}, fillColor = {126, 58, 0}, fillPattern = FillPattern.Solid, extent = {{-10, 15}, {10, -15}}), Rectangle(origin = {-2, 79}, lineColor = {126, 58, 0}, fillColor = {126, 58, 0}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-80, 15}, {80, -15}}), Ellipse(origin = {78, 79}, fillColor = {126, 58, 0}, fillPattern = FillPattern.Solid, extent = {{-10, 15}, {10, -15}}), Ellipse(origin = {78, 79}, fillColor = {81, 36, 0}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-8, 13}, {8, -13}}), Line(origin = {-2, 94}, points = {{-80, 0}, {80, 0}, {80, 0}}), Line(origin = {-2, 64}, points = {{80, 0}, {-80, 0}})}, coordinateSystem(extent = {{-120, 40}, {120, -40}})));


end NaturalGasPipe;
