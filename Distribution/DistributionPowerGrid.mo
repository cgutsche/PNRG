within PNRG.Distribution;

model DistributionPowerGrid

parameter Integer NIn "Number of Inputs" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer NOut "Number of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer prioOut[NOut] "Priority of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  Real Voltage(unit = "V") = 1 "Voltage of grid";
  Real powerDifference(unit = "kW") "Difference between power suply and consumption";
  Real powerInput(unit = "kW");
  Real totalLoad(unit = "kW");
  
  Real powerInputs[NIn];
  Real powerInputsRaw[NIn];
  Real powerRatio[NIn];
  
  PNRG.Interfaces.ElectricalInput electricalInput[NIn] annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.ElectricalOutput electricalOutput[NOut] annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Backend.EnergeticTransitionWithoutActivator t1(arcWeightOut = {powerInput}, maximumSpeed = 1/3600, nIn = NIn, nOut = 1)  annotation(
    Placement(visible = true, transformation(origin = {-36, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.PC p1(enablingPrioOut = prioOut, nIn = 1, nOut = NOut) annotation(
    Placement(visible = true, transformation(origin = {46, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  powerInput = sum(powerInputs);//*t1.power*t1.actualSpeed./t1.maximumSpeed;
  totalLoad = sum(electricalOutput.arcWeight);
  powerDifference = powerInput - totalLoad;
  for i in 1:NIn loop
    powerInputs[i] = electricalInput[i].arcWeight*electricalInput[i].instSpeed/electricalInput[i].maxSpeed;
    powerInputsRaw[i] = electricalInput[i].arcWeight;
    powerRatio[i] = electricalInput[i].instSpeed/electricalInput[i].maxSpeed;
    connect(electricalInput[i], t1.inPlaces[i]) annotation(
      Line(points = {{-31.2, 0}, {34.8, 0}}, thickness = 0.5));
  end for;
  for i in 1:NOut loop
    connect(electricalOutput[i], p1.outTransition[i]) annotation(
      Line(points = {{-31.2, 0}, {34.8, 0}}, thickness = 0.5));
  end for;
  connect(t1.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{-32, 0}, {36, 0}}, thickness = 0.5));
  annotation(
    Icon(graphics = {Rectangle(origin = {0, 80}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 20}, {100, -20}}), Line(origin = {-76.3724, 71.0217}, points = {{-4, -11.9568}, {-2, 10.0432}, {-12, 10.0432}, {-8, 12.0432}, {8, 12.0432}, {12, 10.0432}, {2, 10.0432}, {4, -11.9568}, {-4, -11.9568}, {4, -7.95679}, {-4, -7.95679}, {4, -3.95679}, {-4, -3.95679}, {2, 0.0432145}, {-2, 0.04321}, {2, 4.04321}, {-2, 4.04321}, {2, 8.04321}, {-2, 8.04321}, {2, 10.0432}, {4, 12.0432}, {6, 10.0432}, {8, 12.0432}, {10, 10.0432}, {-2, 10.0432}, {2, 10.0432}, {0, 12.0432}, {-2, 10.0432}, {-4, 12.0432}, {-6, 10.0432}, {-8, 12.0432}, {-10, 10.0432}, {-2, 10.0432}, {2, 8.04321}, {-2, 8.04321}, {2, 4.04321}, {-2, 4.04321}, {2, 0.0432145}, {-2, 0.0432145}, {4, -3.95679}, {-4, -3.95679}, {4, -7.95679}, {-4, -7.95679}, {4, -11.9568}, {4, -11.9568}}), Line(origin = {75.3321, 71.0217}, points = {{-4, -11.9568}, {-2, 10.0432}, {-12, 10.0432}, {-8, 12.0432}, {8, 12.0432}, {12, 10.0432}, {2, 10.0432}, {4, -11.9568}, {-4, -11.9568}, {4, -7.95679}, {-4, -7.95679}, {4, -3.95679}, {-4, -3.95679}, {2, 0.0432145}, {-2, 0.04321}, {2, 4.04321}, {-2, 4.04321}, {2, 8.04321}, {-2, 8.04321}, {2, 10.0432}, {4, 12.0432}, {6, 10.0432}, {8, 12.0432}, {10, 10.0432}, {-2, 10.0432}, {2, 10.0432}, {0, 12.0432}, {-2, 10.0432}, {-4, 12.0432}, {-6, 10.0432}, {-8, 12.0432}, {-10, 10.0432}, {-2, 10.0432}, {2, 8.04321}, {-2, 8.04321}, {2, 4.04321}, {-2, 4.04321}, {2, 0.0432145}, {-2, 0.0432145}, {4, -3.95679}, {-4, -3.95679}, {4, -7.95679}, {-4, -7.95679}, {4, -11.9568}, {4, -11.9568}}), Line(origin = {-1.89405, 81.123}, points = {{-67, 0}, {67, 0}, {67, 0}}), Line(origin = {0.0696685, 80.3301}, points = {{-84.9637, 0.792893}, {-82.9637, -1.20711}, {81.0363, -1.20711}, {85.0363, 0.792893}, {85.0363, 0.792893}}), Text(origin = {-45, 119}, extent = {{-53, 23}, {53, -23}}, textString = "%name")}, coordinateSystem(extent = {{-120, 40}, {120, -40}})));

end DistributionPowerGrid;
