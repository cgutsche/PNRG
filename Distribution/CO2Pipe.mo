within PNRG.Distribution;

model CO2Pipe
parameter Integer NIn "Number of Inputs" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer NOut "Number of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer prioOut[NOut] "Priority of Outputs" annotation(
    Dialog(enable = true, group = "General properties"));
  Real massInput(unit = "kg");
  Real massInputs[NIn];
  Real totalLoad(unit = "kg");
  PNRG.Interfaces.CO2Input co2Input[NIn] annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.CO2Output co2Output[NOut] annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Backend.EnergeticTransitionWithoutActivator t1(arcWeightOut = {massInput}, maximumSpeed = 1/3600, nIn = NIn, nOut = 1)  annotation(
    Placement(visible = true, transformation(origin = {-36, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.PC p1(enablingPrioOut = prioOut, nIn = 1, nOut = NOut) annotation(
    Placement(visible = true, transformation(origin = {46, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  massInput = sum(massInputs);
  totalLoad = sum(co2Output.arcWeight);
  for i in 1:NIn loop
    massInputs[i] = co2Input[i].arcWeight*co2Input[i].instSpeed/co2Input[i].maxSpeed;
    connect(co2Input[i], t1.inPlaces[i]) annotation(
      Line(points = {{-31.2, 0}, {34.8, 0}}, thickness = 0.5));
  end for;
  for i in 1:NOut loop
    connect(co2Output[i], p1.outTransition[i]) annotation(
      Line(points = {{-31.2, 0}, {34.8, 0}}, thickness = 0.5));
  end for;
  connect(t1.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{-32, 0}, {36, 0}}, thickness = 0.5));
  annotation(
    Icon(graphics = {Rectangle(origin = {0, 80}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 20}, {100, -20}}), Text(origin = {-45, 119}, extent = {{-53, 23}, {53, -23}}, textString = "%name"), Ellipse(origin = {-82, 79}, fillColor = {54, 54, 54}, fillPattern = FillPattern.Solid, extent = {{-10, 15}, {10, -15}}), Rectangle(origin = {-2, 79}, fillColor = {54, 54, 54}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-80, 15}, {80, -15}}), Ellipse(origin = {78, 79}, fillColor = {54, 54, 54}, fillPattern = FillPattern.Solid, extent = {{-10, 15}, {10, -15}}), Ellipse(origin = {78, 79}, fillColor = {15, 15, 15}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-8, 13}, {8, -13}}), Line(origin = {-2, 94}, points = {{-80, 0}, {80, 0}, {80, 0}}), Line(origin = {-2, 64}, points = {{80, 0}, {-80, 0}})}, coordinateSystem(extent = {{-120, 40}, {120, -40}})));


end CO2Pipe;
