within PNRG.Distribution;

model DCACConverter
  Real inputPower(unit = "kW");
  Real outputPower(unit = "kW");
  
  parameter Real efficiency "Energy conversion effiency" annotation(
    Dialog(enable = true, group = "General properties"));
  Interfaces.ElectricalInput electricalInput annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.ElectricalOutput electricalOutput annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Backend.EnergeticFlowPlace p1(nIn = 1, nOut = 1)  annotation(
    Placement(visible = true, transformation(origin = {60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Backend.EnergeticTransitionWithoutActivator t11(arcWeightOut = {outputPower}, maximumSpeed = 1/3600, nIn = 1, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {2, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  inputPower = t11.power;
  outputPower = inputPower*efficiency;
  connect(t11.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{6, 0}, {50, 0}}, thickness = 0.5));
  connect(p1.outTransition[1], electricalOutput) annotation(
    Line(points = {{70, 0}, {110, 0}}));
  connect(t11.inPlaces[1], electricalInput) annotation(
    Line(points = {{-2, 0}, {-110, 0}}));
  annotation(
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Text(origin = {-45, 119}, extent = {{-53, 23}, {53, -23}}, textString = "%name"), Line(points = {{-100, -100}, {100, 100}, {100, 100}}), Rectangle(origin = {-49, 75}, lineColor = {255, 200, 0}, fillColor = {255, 200, 0}, fillPattern = FillPattern.Solid, extent = {{-37, 7}, {37, -7}}), Text(origin = {37, -60}, textColor = {255, 200, 0}, extent = {{-227, 108}, {227, -108}}, textString = "~", fontName = "Arial"), Rectangle(origin = {-49, 53}, lineColor = {255, 200, 0}, fillColor = {255, 200, 0}, fillPattern = FillPattern.Solid, extent = {{-37, 7}, {37, -7}})}),
    Diagram(coordinateSystem(extent = {{-120, 100}, {120, -160}})));
end DCACConverter;
