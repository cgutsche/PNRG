within PNRG.Distribution;

model ConditionalConnection
  Real power annotation(
    Dialog(enable = true, group = "General properties"));
  Real currentPower;
  Interfaces.ElectricalInput electricalInput annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.ElectricalOutput electricalOutput annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Backend.EnergeticFlowPlace p1(nIn = 1, nOut = 1)  annotation(
    Placement(visible = true, transformation(origin = {60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalInput logicalInput annotation(
    Placement(visible = true, transformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  PNRG.Logics.SplitLogicalInput splitLogicalInput annotation(
    Placement(visible = true, transformation(origin = {0, -80}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  PNlib.Components.TC t1(arcWeightIn = {currentPower}, arcWeightOut = {currentPower}, firingCon = logicalInput.t == 1, maximumSpeed = 1/3600, nIn = 1, nOut = 1)  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.TC t11(arcWeightIn = {2, 2}, firingCon = false, maximumSpeed = 1/3600, nIn = 2)  annotation(
    Placement(visible = true, transformation(origin = {26, -56}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  currentPower = power*logicalInput.t;
  connect(p1.outTransition[1], electricalOutput) annotation(
    Line(points = {{70, 0}, {110, 0}}));
  connect(logicalInput, splitLogicalInput.logicalInput) annotation(
    Line(points = {{0, -110}, {0, -90}}));
  connect(splitLogicalInput.inhibitor_output, t11.inPlaces[1]) annotation(
    Line(points = {{2, -70}, {2, -56}, {21, -56}}));
  connect(electricalInput, t1.inPlaces[1]) annotation(
    Line(points = {{-110, 0}, {-4, 0}}));
  connect(t1.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{4, 0}, {50, 0}}, thickness = 0.5));
  connect(splitLogicalInput.test_output, t11.inPlaces[2]) annotation(
    Line(points = {{-2, -70}, {-2, -56}, {21, -56}}));
  annotation(
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Text(origin = {-45, 119}, extent = {{-53, 23}, {53, -23}}, textString = "%name"), Rectangle(origin = {-65, 0}, fillColor = {255, 200, 0}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-35, 6}, {35, -6}}), Rectangle(origin = {65, 0}, fillColor = {255, 200, 0}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-35, 6}, {35, -6}}), Rectangle(origin = {-10, 23}, rotation = 45, fillColor = {255, 200, 0}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-35, 6}, {35, -6}})}));


end ConditionalConnection;
