within PNRG.Backend;

model EnergeticFlowPlace
  // The following code is modified Code from PNlib library.
  Real t "marking";
  //****MODIFIABLE PARAMETERS AND VARIABLES BEGIN****//
  parameter Integer nIn(min = 0) = 0 "number of input transitions" annotation(
    Dialog(enable = true, group = "Connector sizing"));
  parameter Integer nOut(min = 0) = 0 "number of output transitions" annotation(
    Dialog(enable = true, group = "Connector sizing"));
  parameter Real arcWeightOutSplit[nOut] = fill(1/nOut, nOut) "Split of input arcs sum to output" annotation(
    Dialog(enable = true, group = "Output arc weights"));
  parameter Real startMarks = 0 "start marks" annotation(
    Dialog(enable = true, group = "Marks"));
  Boolean reStart = false "restart condition" annotation(
    Dialog(enable = true, group = "Marks"));
  parameter Integer N = settings.N "N+1=amount of levels" annotation(
    Dialog(enable = true, group = "Level Concentrations"));
  parameter PNlib.Types.EnablingType enablingType = PNlib.Types.EnablingType.Priority "resolution type of actual conflict (type-1-conflict)" annotation(
    Dialog(enable = true, group = "Enabling"));
  parameter Integer enablingPrioIn[nIn] = 1:nIn "enabling priorities of input transitions" annotation(
    Dialog(enable = if enablingType == PNlib.Types.EnablingType.Probability then false else true, group = "Enabling"));
  parameter Integer enablingPrioOut[nOut] = 1:nOut "enabling priorities of output transitions" annotation(
    Dialog(enable = if enablingType == PNlib.Types.EnablingType.Probability then false else true, group = "Enabling"));
  parameter Real enablingProbIn[nIn] = fill(1/nIn, nIn) "enabling probabilities of input transitions" annotation(
    Dialog(enable = if enablingType == PNlib.Types.EnablingType.Priority then false else true, group = "Enabling"));
  Real power "Power (sum of arcWeightIn)";
  Real actualPower "Power (sum of arcWeightIn weighted with speed)";
  Real actualPowers[nIn] "Power (sum of arcWeightIn weighted with speed)";
  parameter Real enablingProbOut[nOut] = fill(1/nOut, nOut) "enabling probabilities of output transitions" annotation(
    Dialog(enable = if enablingType == PNlib.Types.EnablingType.Priority then false else true, group = "Enabling"));
  //****MODIFIABLE PARAMETERS AND VARIABLES END****//
  Real levelCon "conversion of tokens to level concentration according to M and N of the settings box";
  Boolean showPlaceName = settings.showPlaceName "only for place animation and display (Do not change!)";
  Boolean showCapacity = settings.showCapacity "only for place animation and display (Do not change!)";
  Boolean animateMarking = settings.animateMarking "only for place animation and display (Do not change!)";
  Real color[3] "only for place animation and display (Do not change!)";
  parameter Boolean showTokenFlow = settings.showTokenFlow annotation(
    Dialog(enable = true, group = "Token flow"));
  PNlib.Blocks.tokenFlowCon tokenFlow(nIn = nIn, nOut = nOut, conFiringSumIn = firingSumIn.conFiringSum, conFiringSumOut = firingSumOut.conFiringSum, fireIn = fireIn, fireOut = fireOut, arcWeightIn = arcWeightIn, arcWeightOut = arcWeightOut, instSpeedIn = instSpeedIn, instSpeedOut = instSpeedOut) if showTokenFlow;
  parameter Integer localSeedIn = PNlib.Functions.Random.counter() "Local seed to initialize random number generator for input conflicts" annotation(
    Dialog(enable = true, group = "Random Number Generator"));
  parameter Integer localSeedOut = PNlib.Functions.Random.counter() "Local seed to initialize random number generator for output conflicts" annotation(
    Dialog(enable = true, group = "Random Number Generator"));
protected
  outer PNlib.Components.Settings settings "global settings for animation and display";
  Real minMarks = 0 "minimum capacity";
  Real maxMarks = 10e-15 "maximum capacity";
  Real reStartMarks = 0 "number of marks at restart";
  Real disMarkChange "discrete mark change";
  Real conMarkChange "continuous mark change";
  Real arcWeightIn[nIn] "weights of input arcs";
  Real arcWeightOut[nOut] "weights of output arcs";
  Real instSpeedIn[nIn] "instantaneous speed of input transitions";
  Real instSpeedOut[nOut] "instantaneous speed of output transitions";
  Real maxSpeedIn[nIn] "maximum speed of input transitions";
  Real maxSpeedOut[nOut] "maximum speed of output transitions";
  Real prelimSpeedIn[nIn] "preliminary speed of input transitions";
  Real prelimSpeedOut[nOut] "preliminary speed of output transitions";
  Real tokenscale "only for place animation and display";
  Real t_(start = startMarks, fixed = true) "marking";
  Boolean disMarksInOut(start = false, fixed = true) "discrete marks change";
  Boolean preFireIn[nIn] "pre-value of fireIn";
  Boolean preFireOut[nOut] "pre-value of fireOut";
  Boolean fireIn[nIn](each start = false, each fixed = true) "Does any input transition fire?";
  Boolean fireOut[nOut](each start = false, each fixed = true) "Does any output transition fire?";
  Boolean disTransitionIn[nIn] "Are the input transitions discrete?";
  Boolean disTransitionOut[nOut] "Are the output transitions discrete?";
  Boolean activeIn[nIn] "Are the input transitions active?";
  Boolean activeOut[nOut] "Are the output transitions active?";
  Boolean enabledByInPlaces[nIn] "Are the input transitions enabled by all their input places?";
  //****BLOCKS BEGIN****// since no events are generated within functions!!!
  //enabling discrete transitions
  PNlib.Blocks.enablingInCon enableIn(active = activeIn, delayPassed = delayPassedIn.anytrue, nIn = nIn, arcWeight = arcWeightIn, t = t_, maxMarks = maxMarks, TAein = enabledByInPlaces and activeIn, enablingType = enablingType, enablingPrio = enablingPrioIn, enablingProb = enablingProbIn, disTransition = disTransitionIn, localSeed = localSeedIn, globalSeed = settings.globalSeed);
  PNlib.Blocks.enablingOutCon enableOut(delayPassed = delayPassedOut.anytrue, nOut = nOut, arcWeight = arcWeightOut, t = t_, minMarks = minMarks, TAout = activeOut, enablingType = enablingType, enablingPrio = enablingPrioOut, enablingProb = enablingProbOut, disTransition = disTransitionOut, localSeed = localSeedOut, globalSeed = settings.globalSeed);
  //Does any delay passed of a connected transition?
  PNlib.Blocks.anyTrue delayPassedOut(vec = activeOut and disTransitionOut);
  PNlib.Blocks.anyTrue delayPassedIn(vec = activeIn and disTransitionIn);
  //Does any discrete transition fire?
  PNlib.Blocks.anyTrue disMarksOut(vec = fireOut and disTransitionOut);
  PNlib.Blocks.anyTrue disMarksIn(vec = fireIn and disTransitionIn);
  //Is the place fed by input transitions?
  PNlib.Blocks.anyTrue feeding(vec = preFireIn and not disTransitionIn);
  //Is the place emptied by output transitions?"
  PNlib.Blocks.anyTrue emptying(vec = preFireOut and not disTransitionOut);
  //firing sum calculation
  PNlib.Blocks.firingSumCon firingSumIn(fire = preFireIn, arcWeight = arcWeightIn, instSpeed = instSpeedIn, disTransition = disTransitionIn);
  PNlib.Blocks.firingSumCon firingSumOut(fire = preFireOut, arcWeight = arcWeightOut, instSpeed = maxSpeedOut, disTransition = disTransitionOut);
  //****BLOCKS END****//
  Real decFactorIn[nIn] "decreasing factors for input transitions";
  Real decFactorOut[nOut] "decreasing factors for output transitions";
public
  PNlib.Interfaces.PlaceIn inTransition[nIn](each t = t_, each tint = 1, each maxTokens = maxMarks, each maxTokensint = 1, enable = enableIn.TEin_, each emptied = emptying.anytrue, decreasingFactor = decFactorIn, each disPlace = false, each speedSum = firingSumOut.conFiringSum, fire = fireIn, disTransition = disTransitionIn, active = activeIn, arcWeight = arcWeightIn, instSpeed = instSpeedIn, maxSpeed = maxSpeedIn, prelimSpeed = prelimSpeedIn, enabledByInPlaces = enabledByInPlaces) if nIn > 0 "connector for input transitions" annotation(
    Placement(transformation(extent = {{-114, -10}, {-98, 10}}, rotation = 0), iconTransformation(extent = {{-116, -10}, {-100, 10}})));
  PNlib.Interfaces.PlaceOut outTransition[nOut](each t = t_, each tint = 1, each minTokens = minMarks, each minTokensint = 1, enable = enableOut.TEout_, each fed = feeding.anytrue, decreasingFactor = decFactorOut, each disPlace = false, each arcType = PNlib.Types.ArcType.NormalArc, each speedSum = firingSumIn.conFiringSum, each tokenInOut = pre(disMarksInOut), fire = fireOut, disTransition = disTransitionOut, active = activeOut, arcWeight = arcWeightOut, instSpeed = instSpeedOut, maxSpeed = maxSpeedOut, prelimSpeed = prelimSpeedOut, each testValue = -1, each testValueint = -1, each normalArc = false) if nOut > 0 "connector for output transitions" annotation(
    Placement(transformation(extent = {{100, -10}, {116, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput pc_t = t "connector for Simulink connection" annotation(
    Placement(transformation(extent = {{-36, 68}, {-16, 88}}), iconTransformation(extent = {{-10, -10}, {10, 10}}, rotation = 90, origin = {0, 108})));
equation
  for i in 1:nOut loop
    arcWeightOut[i] = arcWeightOutSplit[i]*actualPower;
  end for;
  for i in 1:nIn loop
    actualPowers[i] = if fireIn[i] then arcWeightIn[i]*instSpeedIn[i]/maxSpeedIn[i] else 0;
  end for;
  power = sum(arcWeightIn);
  actualPower = sum(actualPowers);
//decreasing factor calculation
  (decFactorIn, decFactorOut) = PNlib.Functions.decreasingFactor(nIn = nIn, nOut = nOut, t = t_, minMarks = minMarks, maxMarks = maxMarks, speedIn = firingSumIn.conFiringSum, speedOut = firingSumOut.conFiringSum, maxSpeedIn = maxSpeedIn, maxSpeedOut = maxSpeedOut, prelimSpeedIn = prelimSpeedIn, prelimSpeedOut = prelimSpeedOut, arcWeightIn = arcWeightIn, arcWeightOut = arcWeightOut, firingIn = fireIn and not disTransitionIn, firingOut = fireOut and not disTransitionOut);
//calculation of continuous mark change
  conMarkChange = firingSumIn.conFiringSum - firingSumOut.conFiringSum;
  der(t_) = conMarkChange;
//calculation of discrete mark change
  disMarkChange = firingSumIn.disFiringSum - firingSumOut.disFiringSum;
  disMarksInOut = pre(disMarksOut.anytrue) or pre(disMarksIn.anytrue);
  when {disMarksInOut, reStart} then
    reinit(t_, if reStart then reStartMarks else t_ + pre(disMarkChange));
  end when;
//Conversion of tokens to level concentrations
  levelCon = t*settings.M/N;
  for i in 1:nOut loop
    preFireOut[i] = if disTransitionOut[i] then fireOut[i] else pre(fireOut[i]);
  end for;
  for i in 1:nIn loop
    preFireIn[i] = if disTransitionIn[i] then fireIn[i] else pre(fireIn[i]);
  end for;
  t = noEvent(if t_ < minMarks then minMarks elseif t_ > maxMarks then maxMarks else t_);
//****MAIN END****//
//****ANIMATION BEGIN****//
//scaling of tokens for animation
  tokenscale = t*settings.scale;
  color = if settings.animatePlace then if tokenscale < 100 then {255, 255 - 2.55*tokenscale, 255 - 2.55*tokenscale} else {255, 0, 0} else {255, 255, 255};
//****ANIMATION END****//
//****ERROR MESSENGES BEGIN****//
  assert(PNlib.Functions.OddsAndEnds.prioCheck(enablingPrioIn, nIn) or nIn == 0 or enablingType == PNlib.Types.EnablingType.Probability, "The priorities of the input priorities may be given only once and must be selected from 1 to nIn");
  assert(PNlib.Functions.OddsAndEnds.prioCheck(enablingPrioOut, nOut) or nOut == 0 or enablingType == PNlib.Types.EnablingType.Probability, "The priorities of the output priorities may be given only once and must be selected from 1 to nOut");
  assert(PNlib.Functions.OddsAndEnds.isEqual(sum(enablingProbIn), 1.0, 1e-6) or nIn == 0 or enablingType == PNlib.Types.EnablingType.Priority, "The sum of input enabling probabilities has to be equal to 1");
  assert(PNlib.Functions.OddsAndEnds.isEqual(sum(enablingProbOut), 1.0, 1e-6) or nOut == 0 or enablingType == PNlib.Types.EnablingType.Priority, "The sum of output enabling probabilities has to be equal to 1");
  assert(startMarks >= minMarks and startMarks <= maxMarks, "minMarks<=startMarks<=maxMarks");
//****ERROR MESSENGES END****//
  annotation(
    defaultComponentName = "P1",
    Icon(graphics = {Ellipse(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 98}, {100, -96}}), Ellipse(fillColor = DynamicSelect({255, 255, 255}, color), fillPattern = FillPattern.Solid, extent = {{-88, 86}, {88, -86}}), Text(origin = {-7, -44}, extent = {{-227, 108}, {227, -108}}, textString = "~", fontName = "Arial"), Polygon(origin = {42, -46}, fillPattern = FillPattern.Solid, points = {{-22, 26}, {-22, -26}, {22, 0}, {-22, 26}, {-22, 26}})}),
    Diagram(graphics));
end EnergeticFlowPlace;
