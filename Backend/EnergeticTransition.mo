within PNRG.Backend;

model EnergeticTransition
  parameter Integer nIn(min = 0) = 0 "number of input places" annotation(
    Dialog(enable = true, group = "Connector sizing"));
  parameter Integer nInAct(min = 0) = 0 "number of activators input places" annotation(
    Dialog(enable = true, group = "Connector sizing"));
  parameter Integer nOut(min = 0) = 0 "number of output places" annotation(
    Dialog(enable = true, group = "Connector sizing"));
  //****MODIFIABLE PARAMETERS AND VARIABLES BEGIN****//
  Real power;
  Real maximumSpeed = 1 "maximum speed" annotation(
    Dialog(enable = true, group = "Maximum Speed"));
  Real arcWeightInActivator[nInAct] = fill(1, nInAct) "arc weights of activator input places" annotation(
    Dialog(enable = true, group = "Arc Weights"));
  Real arcWeightOut[nOut] = fill(1, nOut) "arc weights of output places" annotation(
    Dialog(enable = true, group = "Arc Weights"));
  Boolean firingCon = true "additional firing condition" annotation(
    Dialog(enable = true, group = "Firing Condition"));
  //****MODIFIABLE PARAMETERS AND VARIABLES END****//
  Boolean fire "Does the transition fire?";
  Real instantaneousSpeed "instantaneous speed";
  Real actualSpeed = if fire then instantaneousSpeed else 0.0;
  Boolean showTransitionName = settings.showTransitionName "only for transition animation and display (Do not change!)";
  Boolean animateSpeed = settings.animateSpeed "only for transition animation and display (Do not change!)";
  Real color[3] "only for transition animation and display (Do not change!)";
  PNlib.Interfaces.TransitionIn[nIn] inPlaces(each active = activation.active, each fire = fire, arcWeight = arcWeightIn[1:nIn], arcWeightint = arcWeightIntIn[1:nIn], each disTransition = false, each instSpeed = instantaneousSpeed, each prelimSpeed = prelimSpeed, each maxSpeed = maximumSpeed, t = tIn[1:nIn], tint = tIntIn[1:nIn], arcType = arcType[1:nIn], minTokens = minTokens[1:nIn], minTokensint = minTokensInt[1:nIn], fed = fed[1:nIn], disPlace = disPlaceIn[1:nIn], enable = enableIn[1:nIn], speedSum = speedSumIn[1:nIn], decreasingFactor = decreasingFactorIn[1:nIn], testValue = testValue[1:nIn], testValueint = testValueInt[1:nIn], normalArc = normalArc[1:nIn]) if nIn > 0 "connector for input places" annotation(
    Placement(visible = true, transformation(origin = {0, -50}, extent = {{-56, -10}, {-40, 10}}, rotation = 0), iconTransformation(origin = {0, -50}, extent = {{-56, -10}, {-40, 10}}, rotation = 0)));
  PNlib.Interfaces.TransitionOut[nOut] outPlaces(each active = activation.active, each fire = fire, each enabledByInPlaces = true, arcWeight = arcWeightOut, arcWeightint = arcWeightIntOut, each disTransition = false, each instSpeed = instantaneousSpeed, each prelimSpeed = prelimSpeed, each maxSpeed = maximumSpeed, t = tOut, tint = tIntOut, maxTokens = maxTokens, maxTokensint = maxTokensInt, emptied = emptied, disPlace = disPlaceOut, speedSum = speedSumOut, decreasingFactor = decreasingFactorOut) if nOut > 0 "connector for output places" annotation(
    Placement(transformation(extent = {{40, -10}, {56, 10}}, rotation = 0)));
  PNlib.Interfaces.TransitionIn[nInAct] activatorInPlace(each active = activation.active, arcType = arcType[nIn + 1:nIn + nInAct], arcWeight = arcWeightIn[nIn + 1:nIn + nInAct], arcWeightint = arcWeightIntIn[nIn + 1:nIn + nInAct], decreasingFactor = decreasingFactorIn[nIn + 1:nIn + nInAct], disPlace = disPlaceIn[nIn + 1:nIn + nInAct], each disTransition = false, enable = enableIn[nIn + 1:nIn + nInAct], fed = fed[nIn + 1:nIn + nInAct], each fire = fire, each instSpeed = instantaneousSpeed, each maxSpeed = maximumSpeed, minTokens = minTokens[nIn + 1:nIn + nInAct], minTokensint = minTokensInt[nIn + 1:nIn + nInAct], normalArc = normalArc[nIn + 1:nIn + nInAct], each prelimSpeed = prelimSpeed, speedSum = speedSumIn[nIn + 1:nIn + nInAct], t = tIn[nIn + 1:nIn + nInAct], testValue = testValue[nIn + 1:nIn + nInAct], testValueint = testValueInt[nIn + 1:nIn + nInAct], tint = tIntIn[nIn + 1:nIn + nInAct]) annotation(
    Placement(visible = true, transformation(origin = {0, 50}, extent = {{-56, -10}, {-40, 10}}, rotation = 0), iconTransformation(origin = {0, 50}, extent = {{-56, -10}, {-40, 10}}, rotation = 0)));
protected
  outer PNlib.Components.Settings settings "global settings for animation and display";
  Real arcWeightIn[nIn + nInAct] "arc weights of input places" annotation(
    Dialog(enable = true, group = "Arc Weights"));
  Real prelimSpeed "preliminary speed";
  Real tIn[nIn + nInAct] "tokens of input places";
  Real tOut[nOut] "tokens of output places";
  Real minTokens[nIn + nInAct] "minimum tokens of input places";
  Real maxTokens[nOut] "maximum tokens of output places";
  Real speedSumIn[nIn + nInAct] "Input speeds of continuous input places";
  Real speedSumOut[nOut] "Output speeds of continuous output places";
  Real decreasingFactorIn[nIn + nInAct] "decreasing factors of input places";
  Real decreasingFactorOut[nOut] "decreasing factors of output places";
  Real testValue[nIn + nInAct] "test values of test or inhibitor arcs";
  PNlib.Types.ArcType arcType[nIn + nInAct] "type of input arcs 1=normal, 2=real test arc,  3=test arc, 4=real inhibitor arc, 5=inhibitor arc, 6=read arc";
  Integer arcWeightIntIn[nIn + nInAct] "Integer arc weights of discrete input places (for generating events!)";
  Integer arcWeightIntOut[nOut] "Integer arc weights of discrete output places (for generating events!)";
  Integer minTokensInt[nIn + nInAct] "Integer minimum tokens of input places (for generating events!)";
  Integer maxTokensInt[nOut] "Integer maximum tokens of output places (for generating events!)";
  Integer tIntIn[nIn + nInAct] "integer tokens of input places (for generating events!)";
  Integer tIntOut[nOut] "integer tokens of output places (for generating events!)";
  Integer testValueInt[nIn + nInAct] "Integer test values of input arcs (for generating events!)";
  Boolean normalArc[nIn + nInAct] "1=no, 2=yes, i.e. double arc: test and normal arc or inhibitor and normal arc";
  Boolean fed[nIn + nInAct] "Are the input places fed by their input transitions?";
  Boolean emptied[nOut] "Are the output places emptied by their output transitions?";
  Boolean disPlaceIn[nIn + nInAct] "Are the input places discrete?";
  Boolean disPlaceOut[nOut] "Are the output places discrete?";
  Boolean enableIn[nIn + nInAct] "Is the transition enabled by all its discrete input transitions?";
  //****BLOCKS BEGIN****// since no events are generated within functions!!!
  //activation process
  PNlib.Blocks.activationCon activation(testValue = testValue, testValueInt = testValueInt, normalArc = normalArc, nIn = nIn, nOut = nOut, tIn = tIn, tOut = tOut, tIntIn = tIntIn, tIntOut = tIntOut, arcType = arcType, arcWeightIn = arcWeightIn, arcWeightOut = arcWeightOut, arcWeightIntIn = arcWeightIntIn, arcWeightIntOut = arcWeightIntOut, minTokens = minTokens, maxTokens = maxTokens, minTokensInt = minTokensInt, maxTokensInt = maxTokensInt, firingCon = firingCon, fed = fed, emptied = emptied, disPlaceIn = disPlaceIn, disPlaceOut = disPlaceOut);
  //PNlib.Blocks.activationCon activation(testValue = testValue[1:nIn], testValueInt = testValueInt[1:nIn], normalArc = normalArc[1:nIn], nIn = nIn, nOut = nOut, tIn = tIn[1:nIn], tOut = tOut, tIntIn = tIntIn[1:nIn], tIntOut = tIntOut[1:nIn], arcType = arcType[1:nIn], arcWeightIn = arcWeightIn[1:nIn], arcWeightOut = arcWeightOut[1:nIn], arcWeightIntIn = arcWeightIntIn[1:nIn], arcWeightIntOut = arcWeightIntOut[1:nIn], minTokens = minTokens[1:nIn], maxTokens = maxTokens[1:nIn], minTokensInt = minTokensInt[1:nIn], maxTokensInt = maxTokensInt[1:nIn], firingCon = firingCon, fed = fed[1:nIn], emptied = emptied[1:nIn], disPlaceIn = disPlaceIn[1:nIn], disPlaceOut = disPlaceOut[1:nIn]);
  //PNlib.Blocks.activationCon activatorActivation(testValue = testValue[nIn+1:nIn+nInAct], testValueInt = testValueInt[nIn+1:nIn+nInAct], normalArc = normalArc[nIn+1:nIn+nInAct], nIn = nInAct, nOut = nOut, tIn = tIn[nIn+1:nIn+nInAct], tOut = tOut[nIn+1:nIn+nInAct], tIntIn = tIntIn[nIn+1:nIn+nInAct], tIntOut = tIntOut, arcType = arcType[nIn+1:nIn+nInAct], arcWeightIn = arcWeightIn[nIn+1:nIn+nInAct], arcWeightOut = arcWeightOut[nIn+1:nIn+nInAct], arcWeightIntIn = arcWeightIntIn[nIn+1:nIn+nInAct], arcWeightIntOut = arcWeightIntOut[nIn+1:nIn+nInAct], minTokens = minTokens[nIn+1:nIn+nInAct], maxTokens = maxTokens[nIn+1:nIn+nInAct], minTokensInt = minTokensInt[nIn+1:nIn+nInAct], maxTokensInt = maxTokensInt[nIn+1:nIn+nInAct], firingCon = firingCon, fed = fed[nIn+1:nIn+nInAct], emptied = emptied[nIn+1:nIn+nInAct], disPlaceIn = disPlaceIn[nIn+1:nIn+nInAct], disPlaceOut = disPlaceOut[nIn+1:nIn+nInAct]);
  //firing process
  Boolean fire_ = PNlib.Functions.OddsAndEnds.allTrue(/* hack for Dymola 2017 */PNlib.Functions.OddsAndEnds.boolOr(enableIn, not disPlaceIn));
  //****BLOCKS END****//
equation
//****MAIN BEGIN****//
//preliminary speed calculation
  arcWeightIn[nIn + 1:nIn + nInAct] = arcWeightInActivator;
  prelimSpeed = PNlib.Functions.preliminarySpeed(nIn = nIn + nInAct, nOut = nOut, arcWeightIn = arcWeightIn, arcWeightOut = arcWeightOut, speedSumIn = speedSumIn, speedSumOut = speedSumOut, maximumSpeed = maximumSpeed, weaklyInputActiveVec = activation.weaklyInputActiveVec, weaklyOutputActiveVec = activation.weaklyOutputActiveVec);
//firing process
  fire = fire_ and activation.active and not maximumSpeed <= 0;
//instantaneous speed calculation
  instantaneousSpeed = min(min(min(decreasingFactorIn), min(decreasingFactorOut))*maximumSpeed, prelimSpeed);
//****MAIN END****//
//****ANIMATION BEGIN****//
  color = if (fire and settings.animateTransition) then {255, 255, 0} else {255, 255, 255};
//****ANIMATION END****//
//****ERROR MESSENGES BEGIN****//  hier noch Message gleiches Kantengewicht und auch Kante dis Place!!
  power = sum(arcWeightIn[1:nIn]);
  for i in 1:nIn loop
    if disPlaceIn[i] then
      arcWeightIntIn[i] = integer(arcWeightIn[i]);
    else
      arcWeightIntIn[i] = 1;
    end if;
    assert((disPlaceIn[i] and arcWeightIn[i] - arcWeightIntIn[i] <= 0.0) or not disPlaceIn[i], "Input arcs connected to discrete places must have integer weights.");
    assert(arcWeightIn[i] >= 0, "Input arc weights must be positive.");
  end for;
  for i in 1:nOut loop
    if disPlaceOut[i] then
      arcWeightIntOut[i] = integer(arcWeightOut[i]);
    else
      arcWeightIntOut[i] = 1;
    end if;
    assert((disPlaceOut[i] and arcWeightOut[i] - arcWeightIntOut[i] <= 0.0) or not disPlaceOut[i], "Output arcs connected to discrete places must have integer weights.");
    assert(arcWeightOut[i] >= 0, "Output arc weights must be positive.");
  end for;
//****ERROR MESSENGES END****//
  annotation(
    defaultComponentName = "T1",
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-40, 100}, {40, -100}}), Text(origin = {-3, -1}, extent = {{-151, 67}, {151, -67}}, textString = "~", fontName = "Arial")}),
    Diagram);
end EnergeticTransition;
