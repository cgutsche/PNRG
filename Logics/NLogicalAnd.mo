within PNRG.Logics;

model NLogicalAnd
  parameter Integer NIn "Number of Inputs" annotation(
    Dialog(enable = true, group = "General properties"));
  Interfaces.LogicalInput logicalInput[NIn] annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.LogicalOutput logicalOutput annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  LogicalAnd logicalAnd[NIn - 1] annotation(
    Placement(visible = true, transformation(origin = {-2, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(logicalInput[1], logicalAnd[1].logicalInput1);
  connect(logicalInput[2], logicalAnd[1].logicalInput);
  if NIn > 2 then
    for i in 3:NIn loop
      connect(logicalAnd[i - 2].logicalOutput, logicalAnd[i - 1].logicalInput1);
      connect(logicalInput[i], logicalAnd[i - 1].logicalInput);
    end for;
  end if;
  annotation(
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Text(origin = {-7, 45}, extent = {{-109, 97}, {109, -97}}, textString = "N&")}));
end NLogicalAnd;
