within PNRG.Logics;

model LogicalAnd
  PNlib.Components.PD p1(enablingType = PNlib.Types.EnablingType.Priority, maxTokens = 1, nIn = 1, nOut = 3) annotation(
    Placement(visible = true, transformation(origin = {12, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.IA ia annotation(
    Placement(visible = true, transformation(origin = {62.6667, -44.2857}, extent = {{-14.4667, 0.590476}, {4.13333, 7.08571}}, rotation = 0)));
  PNlib.Components.TA ta(realTestArc = false) annotation(
    Placement(visible = true, transformation(origin = {64.0101, -22.9437}, extent = {{-15.8101, 0.64531}, {4.51717, 7.74372}}, rotation = 0)));
  PNlib.Components.T t3(arcWeightIn = {1, 1}, arcWeightOut = {1}, nIn = 2, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {-28, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.T t2(arcWeightIn = {1}, arcWeightOut = {1}, nIn = 1, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {-28, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.T t(arcWeightIn = {1}, arcWeightOut = {1}, nIn = 1, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {-28, 24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.PD p2(enablingType = PNlib.Types.EnablingType.Priority, maxTokens = 1, nIn = 2, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {14, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalOutput logicalOutput annotation(
    Placement(visible = true, transformation(origin = {120, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalInput logicalInput1 annotation(
    Placement(visible = true, transformation(origin = {-108, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalInput logicalInput2 annotation(
    Placement(visible = true, transformation(origin = {-108, -68}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.SplitLogicalInput splitLogicalInput annotation(
    Placement(visible = true, transformation(origin = {-80, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.SplitLogicalInput splitLogicalInput1 annotation(
    Placement(visible = true, transformation(origin = {-78, -68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.T t1(arcWeightIn = {1, 1}, nIn = 2) annotation(
    Placement(visible = true, transformation(origin = {42, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.CombineLogicalOutput combineLogicalOutput annotation(
    Placement(visible = true, transformation(origin = {88, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(t3.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{-23.2, -40}, {0.8, -40}}, thickness = 0.5));
  connect(logicalInput2, splitLogicalInput1.logicalInput) annotation(
    Line(points = {{-108, -68}, {-88, -68}}));
  connect(logicalInput1, splitLogicalInput.logicalInput) annotation(
    Line(points = {{-108, 32}, {-90, 32}}));
  connect(splitLogicalInput.test_output, t3.inPlaces[1]) annotation(
    Line(points = {{-69.2, 34}, {-57.2, 34}, {-57.2, -40}, {-31.2, -40}}));
  connect(splitLogicalInput1.test_output, t3.inPlaces[2]) annotation(
    Line(points = {{-67.2, -66}, {-57.2, -66}, {-57.2, -40}, {-31.2, -40}}));
  connect(splitLogicalInput1.inhibitor_output, t2.inPlaces[1]) annotation(
    Line(points = {{-67.2, -70}, {-37.2, -70}, {-37.2, -2}, {-31.2, -2}}));
  connect(splitLogicalInput.inhibitor_output, t.inPlaces[1]) annotation(
    Line(points = {{-69.2, 30}, {-37.2, 30}, {-37.2, 24}, {-31.2, 24}}));
  connect(t.outPlaces[1], p2.inTransition[1]) annotation(
    Line(points = {{-23.2, 24}, {-9.2, 24}, {-9.2, 8}, {4.8, 8}}, thickness = 0.5));
  connect(t2.outPlaces[1], p2.inTransition[2]) annotation(
    Line(points = {{-23.2, -2}, {-9.2, -2}, {-9.2, 8}, {4.8, 8}}, thickness = 0.5));
  connect(p2.outTransition[1], t1.inPlaces[1]) annotation(
    Line(points = {{24.8, 8}, {38.8, 8}}, thickness = 0.5));
  connect(combineLogicalOutput.logicalInput, logicalOutput) annotation(
    Line(points = {{99, -20}, {119, -20}}, color = {53, 28, 117}));
  connect(p1.outTransition[1], t1.inPlaces[2]) annotation(
    Line(points = {{22.8, -40}, {30.8, -40}, {30.8, 8}, {38.8, 8}}, thickness = 0.5));
  connect(p1.outTransition[2], ta.inPlace) annotation(
    Line(points = {{22.8, -40}, {38.8, -40}, {38.8, -18}, {46.8, -18}}));
  connect(p1.outTransition[3], ia.inPlace) annotation(
    Line(points = {{22.8, -40}, {47.8, -40}}));
  connect(ia.outTransition, combineLogicalOutput.inhibitor_input) annotation(
    Line(points = {{68.2762, -40.4476}, {74.2762, -40.4476}, {74.2762, -22.4476}, {78.2762, -22.4476}}));
  connect(ta.outTransition, combineLogicalOutput.test_input) annotation(
    Line(points = {{70.1405, -18.7492}, {78.1405, -18.7492}}));
  annotation(
    uses(PNlib(version = "2.2")),
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Text(origin = {7, 37}, extent = {{-109, 97}, {109, -97}}, textString = "&")}),
  Diagram(coordinateSystem(extent = {{-140, 80}, {140, -80}})));
end LogicalAnd;
