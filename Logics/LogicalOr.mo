within PNRG.Logics;

model LogicalOr
  PNlib.Components.T t13(arcWeightIn = {1}, arcWeightOut = {1}, nIn = 1, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {-28, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.T t16(arcWeightIn = {1, 1, 1}, nIn = 3) annotation(
    Placement(visible = true, transformation(origin = {42, 48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.T t12(arcWeightIn = {1}, arcWeightOut = {1}, nIn = 1, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {-28, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.PD p14(enablingType = PNlib.Types.EnablingType.Priority, maxTokens = 1, nIn = 2, nOut = 3) annotation(
    Placement(visible = true, transformation(origin = {14, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.IA ia12 annotation(
    Placement(visible = true, transformation(origin = {66.6667, -18.2857}, extent = {{-14.4667, 0.590476}, {4.13333, 7.08571}}, rotation = 0)));
  PNlib.Components.TA ta1(realTestArc = false) annotation(
    Placement(visible = true, transformation(origin = {66.0101, 5.0563}, extent = {{-15.8101, 0.64531}, {4.51717, 7.74372}}, rotation = 0)));
  CombineLogicalOutput combineLogicalOutput annotation(
    Placement(visible = true, transformation(origin = {88, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  SplitLogicalInput splitLogicalInput annotation(
    Placement(visible = true, transformation(origin = {-80, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  SplitLogicalInput splitLogicalInput1 annotation(
    Placement(visible = true, transformation(origin = {-80, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalInput logicalInput annotation(
    Placement(visible = true, transformation(origin = {-110, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalInput logicalInput1 annotation(
    Placement(visible = true, transformation(origin = {-110, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalOutput logicalOutput annotation(
    Placement(visible = true, transformation(origin = {118, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(t12.outPlaces[1], p14.inTransition[2]) annotation(
    Line(points = {{-23, -30}, {-11.4, -30}, {-11.4, 0}, {2.8, 0}}, thickness = 0.5));
  connect(t13.outPlaces[1], p14.inTransition[1]) annotation(
    Line(points = {{-23, 30}, {-11.2, 30}, {-11.2, 0}, {3, 0}}, thickness = 0.5));
  connect(p14.outTransition[3], ia12.inPlace) annotation(
    Line(points = {{24.8, 0}, {38.8, 0}, {38.8, -14}, {51, -14}}));
  connect(p14.outTransition[2], ta1.inPlace) annotation(
    Line(points = {{24.8, 0}, {38.8, 0}, {38.8, 9}, {49.8, 9}}));
  connect(p14.outTransition[1], t16.inPlaces[1]) annotation(
    Line(points = {{24.8, 0}, {31.8, 0}, {31.8, 48}, {36.8, 48}}, thickness = 0.5));
  connect(logicalInput, splitLogicalInput.logicalInput) annotation(
    Line(points = {{-110, 30}, {-90, 30}}));
  connect(logicalInput1, splitLogicalInput1.logicalInput) annotation(
    Line(points = {{-110, -30}, {-90, -30}}));
  connect(combineLogicalOutput.logicalInput, logicalOutput) annotation(
    Line(points = {{100, 0}, {118, 0}}, color = {53, 28, 117}));
  connect(ta1.outTransition, combineLogicalOutput.test_input) annotation(
    Line(points = {{72, 10}, {78, 10}, {78, 2}}));
  connect(ia12.outTransition, combineLogicalOutput.inhibitor_input) annotation(
    Line(points = {{72, -14}, {78, -14}, {78, -2}}));
  connect(splitLogicalInput1.test_output, t12.inPlaces[1]) annotation(
    Line(points = {{-70, -28}, {-32, -28}, {-32, -30}}));
  connect(splitLogicalInput.test_output, t13.inPlaces[1]) annotation(
    Line(points = {{-70, 32}, {-32, 32}, {-32, 30}}));
  connect(splitLogicalInput.inhibitor_output, t16.inPlaces[2]) annotation(
    Line(points = {{-70, 28}, {-52, 28}, {-52, 48}, {38, 48}}));
  connect(splitLogicalInput1.inhibitor_output, t16.inPlaces[3]) annotation(
    Line(points = {{-70, -32}, {-52, -32}, {-52, 48}, {38, 48}}));
protected
  annotation(
    uses(PNlib(version = "2.2")),
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Text(origin = {0, 46}, extent = {{-100, 70}, {100, -70}}, textString = ">=1")}));
end LogicalOr;