within PNRG.Logics;

model LogicalNot
  PNRG.Interfaces.LogicalInput logicalInput annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalOutput logicalOutput annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  SplitLogicalInput splitLogicalInput annotation(
    Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  CombineLogicalOutput combineLogicalOutput annotation(
    Placement(visible = true, transformation(origin = {70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(logicalInput, splitLogicalInput.logicalInput) annotation(
    Line(points = {{-110, 0}, {-80, 0}}));
  connect(combineLogicalOutput.logicalInput, logicalOutput) annotation(
    Line(points = {{82, 0}, {110, 0}}, color = {53, 28, 117}));
  connect(splitLogicalInput.test_output, combineLogicalOutput.inhibitor_input) annotation(
    Line(points = {{-60, 2}, {4, 2}, {4, -2}, {60, -2}}));
  connect(splitLogicalInput.inhibitor_output, combineLogicalOutput.test_input) annotation(
    Line(points = {{-60, -2}, {-4, -2}, {-4, 2}, {60, 2}}));
  annotation(
    uses(PNlib(version = "2.2")),
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Polygon(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, lineThickness = 2.25, points = {{-80, 75}, {20, 0}, {-80, -75}, {-80, 75}}), Ellipse(origin = {47, 1}, lineThickness = 2.25, extent = {{25, 25}, {-25, -25}})}));
end LogicalNot;