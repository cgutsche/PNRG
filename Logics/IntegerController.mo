within PNRG.Logics;

model IntegerController
  parameter Integer NStart "Start Number" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer NChange "Change of Number when active" annotation(
    Dialog(enable = true, group = "General properties"));
  parameter Integer NMax "Maximum Number" annotation(
    Dialog(enable = true, group = "General properties"));
  Integer N "Number";
  parameter Real delay "Minimum delay between Changes" annotation(
    Dialog(enable = true, group = "General properties"));
  PNlib.Components.PD p1(startTokens = NStart, maxTokens = NMax, nIn = 1, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {26, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.PD p11(startTokens = NMax - NStart, maxTokens = NMax, nIn = 1, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {-42, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalInput logicalInput annotation(
    Placement(visible = true, transformation(origin = {-110, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.TD t1(arcWeightIn = {NChange, 1}, arcWeightOut = {NChange}, delay = delay, nIn = 2, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {-8, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.TD t11(arcWeightIn = {NChange, 1}, arcWeightOut = {NChange}, delay = delay, nIn = 2, nOut = 1) annotation(
    Placement(visible = true, transformation(origin = {-8, -22}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.SplitLogicalInput splitLogicalInput annotation(
    Placement(visible = true, transformation(origin = {-70, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Interfaces.LogicalInput logicalInput1 annotation(
    Placement(visible = true, transformation(origin = {-110, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.SplitLogicalInput splitLogicalInput1 annotation(
    Placement(visible = true, transformation(origin = {-64, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNlib.Components.TD t12(arcWeightIn = {1, 1}, delay = delay, nIn = 2, nOut = 0) annotation(
    Placement(visible = true, transformation(origin = {84, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  N = p1.t;
  connect(t1.outPlaces[1], p1.inTransition[1]) annotation(
    Line(points = {{-3.2, 20}, {16.8, 20}}, thickness = 0.5));
  connect(p1.outTransition[1], t11.inPlaces[1]) annotation(
    Line(points = {{36.8, 20}, {54.8, 20}, {54.8, -22}, {-2.2, -22}}, thickness = 0.5));
  connect(t11.outPlaces[1], p11.inTransition[1]) annotation(
    Line(points = {{-12.8, -22}, {-59.8, -22}, {-59.8, 20}, {-51.8, 20}}, thickness = 0.5));
  connect(p11.outTransition[1], t1.inPlaces[1]) annotation(
    Line(points = {{-31.2, 20}, {-11.2, 20}}, thickness = 0.5));
  connect(logicalInput, splitLogicalInput.logicalInput) annotation(
    Line(points = {{-110, 50}, {-81, 50}}));
  connect(splitLogicalInput.test_output, t1.inPlaces[2]) annotation(
    Line(points = {{-59, 52}, {-20, 52}, {-20, 20}, {-13, 20}}));
  connect(logicalInput1, splitLogicalInput1.logicalInput) annotation(
    Line(points = {{-110, -50}, {-75, -50}}));
  connect(splitLogicalInput.inhibitor_output, t12.inPlaces[1]) annotation(
    Line(points = {{-59, 48}, {64, 48}, {64, 0}, {79, 0}}));
  connect(splitLogicalInput1.inhibitor_output, t12.inPlaces[2]) annotation(
    Line(points = {{-53, -52}, {64, -52}, {64, 0}, {79, 0}}));
  connect(splitLogicalInput1.test_output, t11.inPlaces[2]) annotation(
    Line(points = {{-54, -48}, {8, -48}, {8, -22}, {-4, -22}}));
  annotation(
    uses(PNlib(version = "2.2")),
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{100, 100}, {-100, -100}}), Text(origin = {-67, 57}, extent = {{-59, 43}, {59, -43}}, textString = "+"), Text(origin = {-67, -39}, extent = {{-123, 81}, {123, -81}}, textString = "-"), Text(origin = {29, 0}, extent = {{-71, 100}, {71, -100}}, textString = "N=%N")}));
end IntegerController;