within PNRG.Interfaces;

connector LogicalOutput
  import PNlib.Types.ArcType;
  output Boolean active "Are the output transitions active?" annotation(
    HideResult = true);
  output Boolean fire "Do the output transitions fire?" annotation(
    HideResult = true);
  output Real arcWeight "Arc weights of output transitions" annotation(
    HideResult = true);
  output Integer arcWeightint "Integer arc weights of output transitions" annotation(
    HideResult = true);
  output Boolean disTransition "Are the output transitions discrete?" annotation(
    HideResult = true);
  output Real instSpeed "Instantaneous speeds of continuous output transitions" annotation(
    HideResult = true);
  output Real prelimSpeed "Preliminary speeds of continuous output transitions" annotation(
    HideResult = true);
  output Real maxSpeed "Maximum speeds of continuous output transitions" annotation(
    HideResult = true);
  input Real t "Marking of the place" annotation(
    HideResult = true);
  input Integer tint "Integer marking of the place" annotation(
    HideResult = true);
  input Real minTokens "Minimum capacity of the place" annotation(
    HideResult = true);
  input Integer minTokensint "Integer minimum capacity of the place" annotation(
    HideResult = true);
  input Boolean enable "Which of the output transitions are enabled by the place?" annotation(
    HideResult = true);
  input Real decreasingFactor "Factor for decreasing the speed of continuous input transitions" annotation(
    HideResult = true);
  input Boolean disPlace "Type of the place (discrete or continuous)" annotation(
    HideResult = true);
  input ArcType arcType "Type of output arcs (normal, test, inhibition, or read)" annotation(
    HideResult = true);
  input Boolean fed "Is the continuous place fed by input transitions?" annotation(
    HideResult = true);
  input Real speedSum "Input speed of a continuous place" annotation(
    HideResult = true);
  input Boolean tokenInOut "Does the place have a discrete token change?" annotation(
    HideResult = true);
  input Real testValue "Test value of a test or inhibitor arc" annotation(
    HideResult = true);
  input Integer testValueint "Integer test value of a test or inhibitor arc" annotation(
    HideResult = true);
  input Boolean normalArc "Double arc: test and normal arc or inhibitor and normal arc" annotation(
    HideResult = true);
  output Boolean active_inhibitor "Are the output transitions active?" annotation(
    HideResult = true);
  output Boolean fire_inhibitor "Do the output transitions fire?" annotation(
    HideResult = true);
  output Real arcWeight_inhibitor "Arc weights of output transitions" annotation(
    HideResult = true);
  output Integer arcWeightint_inhibitor "Integer arc weights of output transitions" annotation(
    HideResult = true);
  output Boolean disTransition_inhibitor "Are the output transitions discrete?" annotation(
    HideResult = true);
  output Real instSpeed_inhibitor "Instantaneous speeds of continuous output transitions" annotation(
    HideResult = true);
  output Real prelimSpeed_inhibitor "Preliminary speeds of continuous output transitions" annotation(
    HideResult = true);
  output Real maxSpeed_inhibitor "Maximum speeds of continuous output transitions" annotation(
    HideResult = true);
  input Real t_inhibitor "Marking of the place" annotation(
    HideResult = true);
  input Integer tint_inhibitor "Integer marking of the place" annotation(
    HideResult = true);
  input Real minTokens_inhibitor "Minimum capacity of the place" annotation(
    HideResult = true);
  input Integer minTokensint_inhibitor "Integer minimum capacity of the place" annotation(
    HideResult = true);
  input Boolean enable_inhibitor "Which of the output transitions are enabled by the place?" annotation(
    HideResult = true);
  input Real decreasingFactor_inhibitor "Factor for decreasing the speed of continuous input transitions" annotation(
    HideResult = true);
  input Boolean disPlace_inhibitor "Type of the place (discrete or continuous)" annotation(
    HideResult = true);
  input ArcType arcType_inhibitor "Type of output arcs (normal, test, inhibition, or read)" annotation(
    HideResult = true);
  input Boolean fed_inhibitor "Is the continuous place fed by input transitions?" annotation(
    HideResult = true);
  input Real speedSum_inhibitor "Input speed of a continuous place" annotation(
    HideResult = true);
  input Boolean tokenInOut_inhibitor "Does the place have a discrete token change?" annotation(
    HideResult = true);
  input Real testValue_inhibitor "Test value of a test or inhibitor arc" annotation(
    HideResult = true);
  input Integer testValueint_inhibitor "Integer test value of a test or inhibitor arc" annotation(
    HideResult = true);
  input Boolean normalArc_inhibitor "Double arc: test and normal arc or inhibitor and normal arc" annotation(
    HideResult = true);
  annotation(
    Icon(graphics = {Polygon(origin = {14, -26}, lineColor = {53, 28, 117}, fillColor = {53, 28, 117}, fillPattern = FillPattern.Solid, points = {{-114, 126}, {86, 26}, {-114, -74}, {-114, 126}})}));
end LogicalOutput;