within PNRG.Interfaces;

connector NaturalGasInput
  extends PNlib.Interfaces.TransitionIn;
  annotation(
    Icon(graphics = {Polygon(lineColor = {126, 58, 0}, fillColor = {126, 58, 0}, fillPattern = FillPattern.Solid, points = {{-100, 100}, {98, 0}, {-100, -100}, {-100, 100}}), Polygon(origin = {10, -26}, fillColor = {255, 255, 255}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-80, 82}, {34, 26}, {-80, -32}, {-80, 82}})}));
end NaturalGasInput;