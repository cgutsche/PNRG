within PNRG.Interfaces;

connector NaturalGasOutput
  extends PNlib.Interfaces.PlaceOut;
  annotation(
    Icon(graphics = {Polygon(origin = {14, -26}, lineColor = {126, 58, 0}, fillColor = {126, 58, 0}, fillPattern = FillPattern.Solid, points = {{-114, 126}, {86, 26}, {-114, -74}, {-114, 126}})}));
end NaturalGasOutput;