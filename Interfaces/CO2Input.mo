within PNRG.Interfaces;

connector CO2Input
  extends PNlib.Interfaces.TransitionIn;
  annotation(
    Icon(graphics = {Polygon(lineColor = {54, 54, 54}, fillColor = {54, 54, 54}, fillPattern = FillPattern.Solid, points = {{-100, 100}, {98, 0}, {-100, -100}, {-100, 100}}), Polygon(origin = {10, -26}, fillColor = {255, 255, 255}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-80, 82}, {34, 26}, {-80, -32}, {-80, 82}})}));
end CO2Input;
