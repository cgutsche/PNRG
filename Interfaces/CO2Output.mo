within PNRG.Interfaces;

connector CO2Output
  extends PNlib.Interfaces.PlaceOut;
  annotation(
    Icon(graphics = {Polygon(origin = {14, -26}, lineColor = {54, 54, 54}, fillColor = {54, 54, 54}, fillPattern = FillPattern.Solid, points = {{-114, 126}, {86, 26}, {-114, -74}, {-114, 126}})}));
end CO2Output;
