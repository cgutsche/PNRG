within PNRG.Interfaces;

connector LogicalInput
  import PNlib.Types.ArcType;
  output Real t "Markings of input places" annotation(
    HideResult = true);
  output Integer tint "Integer Markings of input places" annotation(
    HideResult = true);
  output Real minTokens "Minimum capacites of input places" annotation(
    HideResult = true);
  output Integer minTokensint "Integer minimum capacites of input places" annotation(
    HideResult = true);
  output Boolean enable "Is the transition enabled by input places?" annotation(
    HideResult = true);
  output Real decreasingFactor "Factor of continuous input places for decreasing the speed" annotation(
    HideResult = true);
  output Boolean disPlace "Types of input places (discrete or continuous)" annotation(
    HideResult = true);
  output ArcType arcType "Types of input arcs (normal, test, inhibition, or read)" annotation(
    HideResult = true);
  output Boolean fed "Are the continuous input places fed?" annotation(
    HideResult = true);
  output Real speedSum "Input speeds of continuous input places" annotation(
    HideResult = true);
  output Boolean tokenInOut "Do the input places have a discrete token change?" annotation(
    HideResult = true);
  output Real testValue "Test value of a test or inhibitor arc" annotation(
    HideResult = true);
  output Integer testValueint "Integer test value of a test or inhibitor arc" annotation(
    HideResult = true);
  output Boolean normalArc "Double arc: test and normal arc or inhibitor and normal arc" annotation(
    HideResult = true);
  input Boolean active "Is the transition active?" annotation(
    HideResult = true);
  input Boolean fire "Does the transition fire?" annotation(
    HideResult = true);
  input Real arcWeight "Input arc weights of the transition" annotation(
    HideResult = true);
  input Integer arcWeightint "Integer input arc weights of the transition" annotation(
    HideResult = true);
  input Boolean disTransition "Type of the transition(discrete/stochastic or continuous)" annotation(
    HideResult = true);
  input Real instSpeed "Instantaneous speed of a continuous transition" annotation(
    HideResult = true);
  input Real prelimSpeed "Preliminary speed of a continuous transition" annotation(
    HideResult = true);
  input Real maxSpeed "Maximum speed of a continuous transition" annotation(
    HideResult = true);
  output Real t_inhibitor "Markings of input places" annotation(
    HideResult = true);
  output Integer tint_inhibitor "Integer Markings of input places" annotation(
    HideResult = true);
  output Real minTokens_inhibitor "Minimum capacites of input places" annotation(
    HideResult = true);
  output Integer minTokensint_inhibitor "Integer minimum capacites of input places" annotation(
    HideResult = true);
  output Boolean enable_inhibitor "Is the transition enabled by input places?" annotation(
    HideResult = true);
  output Real decreasingFactor_inhibitor "Factor of continuous input places for decreasing the speed" annotation(
    HideResult = true);
  output Boolean disPlace_inhibitor "Types of input places (discrete or continuous)" annotation(
    HideResult = true);
  output ArcType arcType_inhibitor "Types of input arcs (normal, test, inhibition, or read)" annotation(
    HideResult = true);
  output Boolean fed_inhibitor "Are the continuous input places fed?" annotation(
    HideResult = true);
  output Real speedSum_inhibitor "Input speeds of continuous input places" annotation(
    HideResult = true);
  output Boolean tokenInOut_inhibitor "Do the input places have a discrete token change?" annotation(
    HideResult = true);
  output Real testValue_inhibitor "Test value of a test or inhibitor arc" annotation(
    HideResult = true);
  output Integer testValueint_inhibitor "Integer test value of a test or inhibitor arc" annotation(
    HideResult = true);
  output Boolean normalArc_inhibitor "Double arc: test and normal arc or inhibitor and normal arc" annotation(
    HideResult = true);
  input Boolean active_inhibitor "Is the transition active?" annotation(
    HideResult = true);
  input Boolean fire_inhibitor "Does the transition fire?" annotation(
    HideResult = true);
  input Real arcWeight_inhibitor "Input arc weights of the transition" annotation(
    HideResult = true);
  input Integer arcWeightint_inhibitor "Integer input arc weights of the transition" annotation(
    HideResult = true);
  input Boolean disTransition_inhibitor "Type of the transition(discrete/stochastic or continuous)" annotation(
    HideResult = true);
  input Real instSpeed_inhibitor "Instantaneous speed of a continuous transition" annotation(
    HideResult = true);
  input Real prelimSpeed_inhibitor "Preliminary speed of a continuous transition" annotation(
    HideResult = true);
  input Real maxSpeed_inhibitor "Maximum speed of a continuous transition" annotation(
    HideResult = true);
  annotation(
    Icon(graphics = {Polygon(lineColor = {53, 28, 117}, fillColor = {53, 28, 117}, fillPattern = FillPattern.Solid, points = {{-100, 100}, {98, 0}, {-100, -100}, {-100, 100}}), Polygon(origin = {10, -24}, fillColor = {255, 255, 255}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-80, 82}, {34, 26}, {-80, -32}, {-80, 82}})}));
end LogicalInput;