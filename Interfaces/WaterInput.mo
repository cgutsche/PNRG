within PNRG.Interfaces;

connector WaterInput
  extends PNlib.Interfaces.TransitionIn;
  annotation(
    Icon(graphics = {Polygon(lineColor = {61, 133, 198}, fillColor = {61, 133, 198}, fillPattern = FillPattern.Solid, points = {{-100, 100}, {98, 0}, {-100, -100}, {-100, 100}}), Polygon(origin = {10, -26}, fillColor = {255, 255, 255}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-80, 82}, {34, 26}, {-80, -32}, {-80, 82}})}));
end WaterInput;