within PNRG.Interfaces;

connector OxygenOutput
  extends PNlib.Interfaces.PlaceOut;
  annotation(
    Icon(graphics = {Polygon(origin = {14, -26}, lineColor = {11, 83, 148}, fillColor = {11, 83, 148}, fillPattern = FillPattern.Solid, points = {{-114, 126}, {86, 26}, {-114, -74}, {-114, 126}})}));
end OxygenOutput;