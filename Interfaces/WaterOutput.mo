within PNRG.Interfaces;

connector WaterOutput
  extends PNlib.Interfaces.PlaceOut;
  annotation(
    Icon(graphics = {Polygon(origin = {14, -26}, lineColor = {61, 133, 198}, fillColor = {61, 133, 198}, fillPattern = FillPattern.Solid, points = {{-114, 126}, {86, 26}, {-114, -74}, {-114, 126}})}));
end WaterOutput;