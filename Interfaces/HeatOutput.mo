within PNRG.Interfaces;

connector HeatOutput
  extends PNlib.Interfaces.PlaceOut;
  annotation(
    Icon(graphics = {Polygon(origin = {14, -26}, lineColor = {255, 80, 50}, fillColor = {255, 80, 50}, fillPattern = FillPattern.Solid, points = {{-114, 126}, {86, 26}, {-114, -74}, {-114, 126}})}));
end HeatOutput;