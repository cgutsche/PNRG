within PNRG.Interfaces;

connector HydrogenOutput
  extends PNlib.Interfaces.PlaceOut;
  annotation(
    Icon(graphics = {Polygon(origin = {14, -26}, lineColor = {106, 168, 79}, fillColor = {106, 168, 79}, fillPattern = FillPattern.Solid, points = {{-114, 126}, {86, 26}, {-114, -74}, {-114, 126}})}));
end HydrogenOutput;