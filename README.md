# PNRG

The Petri Net Renewable Energy Grids library (PNRG) is a library for modeling energy systems based on Hybrid Petri net. The models are based on the Petri net implementation of the [PNlib library](https://github.com/AMIT-HSBI/PNlib/tree/master).

The goal is to model energy systems in different states and to control the state transitions within Modelica using the same Petri net structure that is used for the model itself.

## License

This Modelica package is free software and the use is completely at your own risk; it can be redistributed and/or modified under the terms of the [Modelica License 2](https://modelica.org/licenses/ModelicaLicense2/).


## Contact

For questions, remarks and suggestions please send me an e-mail: christian.gutsche@tu-dresden.de