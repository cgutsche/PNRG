within PNRG.Examples;

model simpleExample
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput(NOut = 1, fileName = "P:/Programs/PNRG/data.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-130, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput1(NOut = 1, fileName = "P:/Programs/PNRG/data2.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-130, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput111(NOut = 1, fileName = "P:/Programs/PNRG/data3.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Distribution.Transformer transformer(efficiency = 0.999) annotation(
    Placement(visible = true, transformation(origin = {-54, -20}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  PNRG.Distribution.Transformer transformer1(efficiency = 0.999) annotation(
    Placement(visible = true, transformation(origin = {-54, -44}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  PNRG.PowerPlants.PVPowerPlant pVPowerPlant(areaPV = 100, efficiency_PV = 0.2) annotation(
    Placement(visible = true, transformation(origin = {-80, -44}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.EnergyConsumer.ElectricityConsumer electricityConsumer annotation(
    Placement(visible = true, transformation(origin = {150, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.PowerPlants.WindPowerPlant windPowerPlant(efficiencyTurbine = 0.4, number = testController.N, rotorLength = 3) annotation(
    Placement(visible = true, transformation(origin = {-80, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Storage.Battery battery(power = 10)  annotation(
    Placement(visible = true, transformation(origin = {64, -12}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression(NOut = 2, expression = distributionPowerGrid.powerInput > electricityConsumer.powerConsumption + electricityConsumer1.powerConsumption + battery.power) annotation(
    Placement(visible = true, transformation(origin = {10, 98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression1(NOut = 2, expression = windPowerPlant.currentPower + pVPowerPlant.currentPower <= electricityConsumer.powerConsumption + electricityConsumer1.powerConsumption+ battery.currentInputPower) annotation(
    Placement(visible = true, transformation(origin = {-10, 52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.IntegerController testController(NChange = 1, NMax = 10, NStart = 10, delay = 300) annotation(
    Placement(visible = true, transformation(origin = {-46, 20}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Logics.LogicalExpression logicalExpression11(NOut = 1, expression = windPowerPlant.currentPower + pVPowerPlant.currentPower - windPowerPlant.singlePower > electricityConsumer.powerConsumption + electricityConsumer1.powerConsumption + battery.power) annotation(
    Placement(visible = true, transformation(origin = {-80, 78}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression111(NOut = 1, expression = testController.N == testController.NMax) annotation(
    Placement(visible = true, transformation(origin = {26, 64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalAnd logicalAnd1 annotation(
    Placement(visible = true, transformation(origin = {40, 30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Distribution.DistributionPowerGrid distributionPowerGrid(NIn = 3, NOut = 3, prioOut = {1, 2, 3})  annotation(
    Placement(visible = true, transformation(origin = {64, -54}, extent = {{-30, 10}, {30, 30}}, rotation = 0)));
  Sources.FileToTransitionOutput fileToTransitionOutput11(NOut = 1, fileName = "P:/Programs/PNRG/data4.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {110, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  EnergyConsumer.ElectricityConsumer electricityConsumer1 annotation(
    Placement(visible = true, transformation(origin = {150, -44}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalAnd logicalAnd annotation(
    Placement(visible = true, transformation(origin = {-50, 56}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Logics.LogicalOr logicalOr annotation(
    Placement(visible = true, transformation(origin = {82, 40}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Logics.LogicalExpression logicalExpression2(NOut = 1, expression = testController.N < testController.NMax) annotation(
    Placement(visible = true, transformation(origin = {58, 76}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(fileToTransitionOutput.fileOutput[1], pVPowerPlant.fileInput) annotation(
    Line(points = {{-119, 50}, {-119, 50.6875}, {-109, 50.6875}, {-109, -44}, {-91, -44}}, color = {150, 150, 150}));
  connect(pVPowerPlant.electricalOutput, transformer1.electricalInput) annotation(
    Line(points = {{-69, -44}, {-61, -44}}, color = {255, 200, 0}));
  connect(fileToTransitionOutput1.fileOutput[1], windPowerPlant.fileInput) annotation(
    Line(points = {{-119, 80}, {-100, 80}, {-100, -20}, {-91, -20}}, color = {150, 150, 150}));
  connect(windPowerPlant.electricalOutput, transformer.electricalInput) annotation(
    Line(points = {{-69, -20}, {-61, -20}}, color = {255, 200, 0}));
  connect(fileToTransitionOutput111.fileOutput[1], electricityConsumer.fileInput) annotation(
    Line(points = {{121, 80}, {132, 80}, {132, -8}, {139, -8}}));
  connect(logicalAnd1.logicalOutput, battery.logicalInput1) annotation(
    Line(points = {{40, 19}, {40, -6}, {53, -6}}, color = {53, 28, 117}));
  connect(logicalExpression111.logicalOutput[1], logicalAnd1.logicalInput1) annotation(
    Line(points = {{37, 64}, {37, 63}, {45, 63}, {45, 42}, {46, 42}}, color = {53, 28, 117}));
  connect(logicalExpression1.logicalOutput[1], logicalAnd1.logicalInput2) annotation(
    Line(points = {{1, 52}, {36, 52}, {36, 42}}, color = {53, 28, 117}));
  connect(logicalExpression1.logicalOutput[2], testController.logicalInput) annotation(
    Line(points = {{1, 52}, {11, 52}, {11, 36}, {-41, 36}, {-41, 31}}, color = {53, 28, 117}));
  connect(transformer.electricalOutput, distributionPowerGrid.electricalInput[1]) annotation(
    Line(points = {{-48, -20}, {0, -20}, {0, -34}, {36.5, -34}}, color = {255, 200, 0}));
  connect(transformer1.electricalOutput, distributionPowerGrid.electricalInput[2]) annotation(
    Line(points = {{-48, -44}, {0, -44}, {0, -34}, {36.5, -34}}, color = {255, 200, 0}));
  connect(battery.electricalOutput, distributionPowerGrid.electricalInput[3]) annotation(
    Line(points = {{53, -12}, {18, -12}, {18, -34}, {36.5, -34}}, color = {255, 200, 0}));
  connect(distributionPowerGrid.electricalOutput[2], electricityConsumer.electricalInput) annotation(
    Line(points = {{92, -34}, {112, -34}, {112, -14}, {140, -14}}, color = {255, 200, 0}));
  connect(distributionPowerGrid.electricalOutput[3], battery.electricalInput) annotation(
    Line(points = {{92, -34}, {112, -34}, {112, -12}, {75, -12}}, color = {255, 200, 0}));
  connect(distributionPowerGrid.electricalOutput[1], electricityConsumer1.electricalInput) annotation(
    Line(points = {{92, -34}, {112, -34}, {112, -44}, {140, -44}}, color = {255, 200, 0}));
  connect(fileToTransitionOutput11.fileOutput[1], electricityConsumer1.fileInput) annotation(
    Line(points = {{121, 50}, {128, 50}, {128, -38}, {139, -38}}, color = {150, 150, 150}));
  connect(logicalAnd.logicalOutput, testController.logicalInput1) annotation(
    Line(points = {{-50, 45}, {-50.5, 45}, {-50.5, 31}, {-51, 31}}, color = {53, 28, 117}));
  connect(logicalExpression.logicalOutput[2], logicalAnd.logicalInput1) annotation(
    Line(points = {{22, 98}, {30, 98}, {30, 78}, {-45, 78}, {-45, 67}}, color = {53, 28, 117}));
  connect(logicalExpression11.logicalOutput[1], logicalAnd.logicalInput2) annotation(
    Line(points = {{-69, 78}, {-55, 78}, {-55, 67}}, color = {53, 28, 117}));
  connect(logicalOr.logicalOutput, battery.logicalInput) annotation(
    Line(points = {{82, 30}, {82, -6}, {76, -6}}, color = {53, 28, 117}));
  connect(logicalExpression.logicalOutput[1], logicalOr.logicalInput) annotation(
    Line(points = {{22, 98}, {86, 98}, {86, 52}}, color = {53, 28, 117}));
  connect(logicalExpression2.logicalOutput[1], logicalOr.logicalInput1) annotation(
    Line(points = {{70, 76}, {80, 76}, {80, 52}}, color = {53, 28, 117}));
  annotation(
    Diagram(coordinateSystem(extent = {{-160, 120}, {180, -60}})),
    version = "",
    uses(PNlib(version = "2.2")));
end simpleExample;
