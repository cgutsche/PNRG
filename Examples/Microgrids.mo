within PNRG.Examples;

model Microgrids
  PNRG.Logics.LogicalAnd logicalAnd annotation(
    Placement(visible = true, transformation(origin = {-578, 14}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput1(NOut = 1, fileName = "P:/Programs/PNRG_test/PNRG/data.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-652, -204}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression11(NOut = 1, expression = windPowerPlant.currentPower + conditionalConnection.currentPower - windPowerPlant.singlePower > electricityConsumer1.powerConsumption + battery.power) annotation(
    Placement(visible = true, transformation(origin = {-606, 38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput(NOut = 1, fileName = "P:/Programs/PNRG/data2.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-652, -180}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression(NOut = 2, expression = windPowerPlant.currentPower + conditionalConnection.currentPower > electricityConsumer1.powerConsumption + battery.power) annotation(
    Placement(visible = true, transformation(origin = {-516, 58}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression10(NOut = 1, expression = electricityConsumer.powerConsumption + battery1.currentInputPower - battery1.currentOutputPower - windPowerPlant1.currentPower - pVPowerPlant1.currentPower > electricityConsumer1.powerConsumption - windPowerPlant.currentPower + battery.currentInputPower - battery.currentOutputPower) annotation(
    Placement(visible = true, transformation(origin = {-442, -162}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalOr logicalOr annotation(
    Placement(visible = true, transformation(origin = {-444, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Logics.LogicalExpression logicalExpression2(NOut = 1, expression = testController.N < testController.NMax) annotation(
    Placement(visible = true, transformation(origin = {-468, 36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Distribution.ConditionalConnection conditionalConnection1(power = distributionPowerGrid2.powerInput) annotation(
    Placement(visible = true, transformation(origin = {-514, -146}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.PowerPlants.PVPowerPlant pVPowerPlant(areaPV = 200, efficiency_PV = 0.3) annotation(
    Placement(visible = true, transformation(origin = {-618, -130}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression3(NOut = 1, expression = integerController.N < integerController.NMax) annotation(
    Placement(visible = true, transformation(origin = {-500, -260}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Distribution.ConditionalConnection conditionalConnection(power = distributionPowerGrid2.powerInput) annotation(
    Placement(visible = true, transformation(origin = {-514, -114}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalAnd logicalAnd3 annotation(
    Placement(visible = true, transformation(origin = {-518, -314}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput111(NOut = 1, fileName = "P:/Programs/PNRG/data3.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-386, -168}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.PowerPlants.PVPowerPlant pVPowerPlant1(areaPV = 30) annotation(
    Placement(visible = true, transformation(origin = {-604, -204}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.EnergyConsumer.ElectricityConsumer electricityConsumer1 annotation(
    Placement(visible = true, transformation(origin = {-386, -102}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Storage.Battery battery1(power = 10) annotation(
    Placement(visible = true, transformation(origin = {-456, -222}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Distribution.DistributionPowerGrid distributionPowerGrid(NIn = 3, NOut = 2, Voltage = 240, prioOut = {1, 2}) annotation(
    Placement(visible = true, transformation(origin = {-467, -121}, extent = {{-29, 9.66666}, {29, 29}}, rotation = 0)));
  PNRG.Logics.LogicalAnd logicalAnd2 annotation(
    Placement(visible = true, transformation(origin = {-608, -288}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Distribution.DistributionPowerGrid distributionPowerGrid1(NIn = 4, NOut = 2, Voltage = 240, prioOut = {1, 2}) annotation(
    Placement(visible = true, transformation(origin = {-453, -209}, extent = {{-29, 9.66666}, {29, 29}}, rotation = 0)));
  PNRG.Logics.LogicalAnd logicalAnd1 annotation(
    Placement(visible = true, transformation(origin = {-486, -10}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Logics.LogicalExpression logicalExpression12(NOut = 1, expression = not (not (electricityConsumer.powerConsumption + battery1.currentInputPower - battery1.currentOutputPower - windPowerPlant1.currentPower - pVPowerPlant1.currentPower > electricityConsumer1.powerConsumption - windPowerPlant.currentPower + battery.currentInputPower - battery.currentOutputPower) and distributionPowerGrid.powerDifference < 10)) annotation(
    Placement(visible = true, transformation(origin = {-622, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalAnd logicalAnd4 annotation(
    Placement(visible = true, transformation(origin = {-584, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Distribution.DistributionPowerGrid distributionPowerGrid2(NIn = 1, NOut = 2, Voltage = 240, prioOut = {1, 2}) annotation(
    Placement(visible = true, transformation(origin = {-558, -148}, extent = {{-26, 8.66667}, {26, 26}}, rotation = 0)));
  PNRG.PowerPlants.WindPowerPlant windPowerPlant(efficiencyTurbine = 0.4, number = testController.N, rotorLength = 3) annotation(
    Placement(visible = true, transformation(origin = {-618, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalOr logicalOr1 annotation(
    Placement(visible = true, transformation(origin = {-464, -296}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Logics.LogicalExpression logicalExpression4(NOut = 1, expression = integerController.N == integerController.NMax) annotation(
    Placement(visible = true, transformation(origin = {-532, -272}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput2(NOut = 1, fileName = "P:/Programs/PNRG_test/PNRG/data.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-652, -130}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Storage.Battery battery(power = 10) annotation(
    Placement(visible = true, transformation(origin = {-466, -74}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.IntegerController integerController(NChange = 1, NMax = 8, NStart = 8, delay = 0.1) annotation(
    Placement(visible = true, transformation(origin = {-604, -316}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Logics.LogicalOr logicalOr2 annotation(
    Placement(visible = true, transformation(origin = {-560, -36}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Logics.LogicalExpression logicalExpression1(NOut = 1, expression = windPowerPlant.currentPower + conditionalConnection.currentPower <= electricityConsumer1.powerConsumption) annotation(
    Placement(visible = true, transformation(origin = {-524, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput11(NOut = 1, fileName = "P:/Programs/PNRG/data4.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-386, -80}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Distribution.Transformer transformer1(efficiency = 0.999) annotation(
    Placement(visible = true, transformation(origin = {-594, -130}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  PNRG.Distribution.Transformer transformer2(efficiency = 0.999) annotation(
    Placement(visible = true, transformation(origin = {-578, -204}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression14(NOut = 1, expression = windPowerPlant.currentPower + conditionalConnection.currentPower <= electricityConsumer1.powerConsumption + battery.power) annotation(
    Placement(visible = true, transformation(origin = {-548, 12}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.IntegerController testController(NChange = 1, NMax = 10, NStart = 10, delay = 0.1) annotation(
    Placement(visible = true, transformation(origin = {-566, -70}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNRG.Logics.LogicalExpression logicalExpression111(NOut = 1, expression = testController.N == testController.NMax) annotation(
    Placement(visible = true, transformation(origin = {-500, 24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression7(NOut = 1, expression = windPowerPlant1.currentPower + conditionalConnection1.currentPower - windPowerPlant1.singlePower > electricityConsumer.powerConsumption + battery1.power) annotation(
    Placement(visible = true, transformation(origin = {-638, -258}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.EnergyConsumer.ElectricityConsumer electricityConsumer annotation(
    Placement(visible = true, transformation(origin = {-386, -190}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression6(NOut = 1, expression = not (electricityConsumer.powerConsumption + battery1.currentInputPower - battery1.currentOutputPower - windPowerPlant1.currentPower - pVPowerPlant1.currentPower > electricityConsumer1.powerConsumption - windPowerPlant.currentPower + battery.currentInputPower - battery.currentOutputPower)) annotation(
    Placement(visible = true, transformation(origin = {-442, -128}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Distribution.Transformer transformer(efficiency = 0.999) annotation(
    Placement(visible = true, transformation(origin = {-592, -90}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  PNRG.PowerPlants.WindPowerPlant windPowerPlant1(efficiencyTurbine = 0.4, number = integerController.N, rotorLength = 2) annotation(
    Placement(visible = true, transformation(origin = {-604, -180}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Distribution.Transformer transformer4(efficiency = 0.999) annotation(
    Placement(visible = true, transformation(origin = {-578, -180}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression5(NOut = 2, expression = windPowerPlant1.currentPower + conditionalConnection1.currentPower + pVPowerPlant1.currentPower > electricityConsumer.powerConsumption + battery1.power) annotation(
    Placement(visible = true, transformation(origin = {-548, -238}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression13(NOut = 1, expression = windPowerPlant1.currentPower + pVPowerPlant1.currentPower + conditionalConnection1.currentPower <= electricityConsumer.powerConsumption + battery1.power) annotation(
    Placement(visible = true, transformation(origin = {-572, -294}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput4(NOut = 1, fileName = "P:/Programs/PNRG/data2.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-652, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression8(NOut = 1, expression = windPowerPlant1.currentPower + pVPowerPlant1.currentPower + conditionalConnection1.currentPower <= electricityConsumer.powerConsumption) annotation(
    Placement(visible = true, transformation(origin = {-550, -294}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression9(NOut = 1, expression = not (electricityConsumer.powerConsumption + battery1.power - windPowerPlant1.currentPower - pVPowerPlant1.currentPower > electricityConsumer1.powerConsumption + battery.power - windPowerPlant.currentPower) and distributionPowerGrid1.powerDifference < 0) annotation(
    Placement(visible = true, transformation(origin = {-510, -14}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(logicalExpression5.logicalOutput[2], logicalAnd2.logicalInput1) annotation(
    Line(points = {{-537, -238}, {-529, -238}, {-529, -258}, {-604, -258}, {-604, -277}}, color = {53, 28, 117}));
  connect(logicalExpression10.logicalOutput[1], conditionalConnection1.logicalInput) annotation(
    Line(points = {{-453, -162}, {-514, -162}, {-514, -157}}, color = {53, 28, 117}));
  connect(logicalOr1.logicalOutput, battery1.logicalInput) annotation(
    Line(points = {{-464, -307}, {-464, -312}, {-430, -312}, {-430, -216}, {-444, -216}}, color = {53, 28, 117}));
  connect(transformer4.electricalOutput, distributionPowerGrid1.electricalInput[1]) annotation(
    Line(points = {{-571.4, -180}, {-503.4, -180}, {-503.4, -190}, {-480, -190}}, color = {255, 200, 0}));
  connect(transformer.electricalOutput, distributionPowerGrid.electricalInput[1]) annotation(
    Line(points = {{-585.4, -90}, {-499.4, -90}, {-499.4, -102}, {-494, -102}}, color = {255, 200, 0}));
  connect(pVPowerPlant.electricalOutput, transformer1.electricalInput) annotation(
    Line(points = {{-607, -130}, {-601, -130}}, color = {255, 200, 0}));
  connect(logicalExpression111.logicalOutput[1], logicalAnd1.logicalInput1) annotation(
    Line(points = {{-489, 24}, {-489, 23}, {-481, 23}, {-481, 2}, {-480, 2}}, color = {53, 28, 117}));
  connect(fileToTransitionOutput111.fileOutput[1], electricityConsumer.fileInput) annotation(
    Line(points = {{-397, -168}, {-403, -168}, {-403, -184}, {-397, -184}}, color = {150, 150, 150}));
  connect(logicalExpression1.logicalOutput[1], logicalAnd1.logicalInput2) annotation(
    Line(points = {{-513, 12}, {-490, 12}, {-490, 2}}, color = {53, 28, 117}));
  connect(logicalExpression4.logicalOutput[1], logicalAnd3.logicalInput1) annotation(
    Line(points = {{-521, -272}, {-521, -273}, {-513, -273}, {-513, -303}}, color = {53, 28, 117}));
  connect(logicalOr2.logicalOutput, testController.logicalInput) annotation(
    Line(points = {{-560, -47}, {-560, -58}}, color = {53, 28, 117}));
  connect(logicalOr.logicalOutput, battery.logicalInput) annotation(
    Line(points = {{-444, -11}, {-444, -68}, {-454, -68}}, color = {53, 28, 117}));
  connect(pVPowerPlant1.electricalOutput, transformer2.electricalInput) annotation(
    Line(points = {{-593, -204}, {-585, -204}}, color = {255, 200, 0}));
  connect(windPowerPlant1.electricalOutput, transformer4.electricalInput) annotation(
    Line(points = {{-593, -180}, {-585, -180}}, color = {255, 200, 0}));
  connect(distributionPowerGrid.electricalOutput[1], electricityConsumer1.electricalInput) annotation(
    Line(points = {{-440.417, -101.667}, {-437.668, -101.667}, {-437.668, -101.333}, {-397.584, -101.333}}, color = {255, 200, 0}));
  connect(logicalExpression9.logicalOutput[1], logicalOr2.logicalInput) annotation(
    Line(points = {{-521, -14}, {-557, -14}, {-557, -25}}, color = {53, 28, 117}));
  connect(logicalExpression8.logicalOutput[1], logicalAnd3.logicalInput2) annotation(
    Line(points = {{-539, -294}, {-522.5, -294}, {-522.5, -303}, {-523, -303}}, color = {53, 28, 117}));
  connect(logicalAnd2.logicalOutput, integerController.logicalInput1) annotation(
    Line(points = {{-608, -299}, {-608.5, -299}, {-608.5, -305}, {-609, -305}}, color = {53, 28, 117}));
  connect(fileToTransitionOutput1.fileOutput[1], pVPowerPlant1.fileInput) annotation(
    Line(points = {{-641, -204}, {-615, -204}}, color = {150, 150, 150}));
  connect(fileToTransitionOutput2.fileOutput[1], pVPowerPlant.fileInput) annotation(
    Line(points = {{-641, -130}, {-629, -130}}, color = {150, 150, 150}));
  connect(logicalExpression11.logicalOutput[1], logicalAnd.logicalInput2) annotation(
    Line(points = {{-595, 38}, {-583, 38}, {-583, 25}}, color = {53, 28, 117}));
  connect(battery1.electricalOutput, distributionPowerGrid1.electricalInput[4]) annotation(
    Line(points = {{-467, -222}, {-487, -222}, {-487, -190}, {-480, -190}}, color = {255, 200, 0}));
  connect(conditionalConnection1.electricalOutput, distributionPowerGrid1.electricalInput[3]) annotation(
    Line(points = {{-503, -146}, {-497, -146}, {-497, -190}, {-480, -190}}, color = {255, 200, 0}));
  connect(logicalExpression14.logicalOutput[1], logicalOr2.logicalInput1) annotation(
    Line(points = {{-559, 12}, {-563, 12}, {-563, -24}}, color = {53, 28, 117}));
  connect(battery.electricalOutput, distributionPowerGrid.electricalInput[3]) annotation(
    Line(points = {{-477, -74}, {-497, -74}, {-497, -102}, {-494, -102}}, color = {255, 200, 0}));
  connect(logicalExpression12.logicalOutput[1], logicalAnd4.logicalInput2) annotation(
    Line(points = {{-611, -14}, {-589, -14}, {-589, -18}}, color = {53, 28, 117}));
  connect(distributionPowerGrid2.electricalOutput[2], conditionalConnection1.electricalInput) annotation(
    Line(points = {{-534.167, -130.667}, {-534.167, -130.333}, {-528.167, -130.333}, {-528.167, -146}, {-525, -146}}, color = {255, 200, 0}));
  connect(logicalAnd.logicalOutput, logicalAnd4.logicalInput1) annotation(
    Line(points = {{-578, 3}, {-578, -18}}, color = {53, 28, 117}));
  connect(logicalAnd3.logicalOutput, battery1.logicalInput1) annotation(
    Line(points = {{-518, -325}, {-519, -325}, {-519, -334}, {-480, -334}, {-480, -216}, {-466, -216}}, color = {53, 28, 117}));
  connect(distributionPowerGrid1.electricalOutput[2], battery1.electricalInput) annotation(
    Line(points = {{-426.417, -189.667}, {-416.584, -189.667}, {-416.584, -222.333}, {-444.583, -222.333}}, color = {255, 200, 0}));
  connect(logicalExpression.logicalOutput[2], logicalAnd.logicalInput1) annotation(
    Line(points = {{-505, 58}, {-497, 58}, {-497, 38}, {-573, 38}, {-573, 25}}, color = {53, 28, 117}));
  connect(logicalExpression.logicalOutput[1], logicalOr.logicalInput) annotation(
    Line(points = {{-505, 58}, {-441, 58}, {-441, 12}}, color = {53, 28, 117}));
  connect(transformer2.electricalOutput, distributionPowerGrid1.electricalInput[2]) annotation(
    Line(points = {{-571.4, -204}, {-503.4, -204}, {-503.4, -190}, {-480, -190}}, color = {255, 200, 0}));
  connect(windPowerPlant.electricalOutput, transformer.electricalInput) annotation(
    Line(points = {{-607, -90}, {-599, -90}}, color = {255, 200, 0}));
  connect(fileToTransitionOutput11.fileOutput[1], electricityConsumer1.fileInput) annotation(
    Line(points = {{-397, -80}, {-403, -80}, {-403, -96}, {-397, -96}}, color = {150, 150, 150}));
  connect(logicalExpression13.logicalOutput[1], integerController.logicalInput) annotation(
    Line(points = {{-583, -294}, {-599, -294}, {-599, -304}}, color = {53, 28, 117}));
  connect(logicalExpression5.logicalOutput[1], logicalOr1.logicalInput) annotation(
    Line(points = {{-537, -238}, {-461, -238}, {-461, -285}}, color = {53, 28, 117}));
  connect(logicalExpression2.logicalOutput[1], logicalOr.logicalInput1) annotation(
    Line(points = {{-457, 36}, {-447, 36}, {-447, 12}}, color = {53, 28, 117}));
  connect(fileToTransitionOutput.fileOutput[1], windPowerPlant1.fileInput) annotation(
    Line(points = {{-641, -180}, {-615, -180}}, color = {150, 150, 150}));
  connect(logicalExpression3.logicalOutput[1], logicalOr1.logicalInput1) annotation(
    Line(points = {{-489, -260}, {-467, -260}, {-467, -285}}, color = {53, 28, 117}));
  connect(logicalAnd1.logicalOutput, battery.logicalInput1) annotation(
    Line(points = {{-486, -21}, {-486, -68}, {-476, -68}}, color = {53, 28, 117}));
  connect(logicalExpression6.logicalOutput[1], conditionalConnection.logicalInput) annotation(
    Line(points = {{-453, -128}, {-515, -128}, {-515, -124}}, color = {53, 28, 117}));
  connect(conditionalConnection.electricalOutput, distributionPowerGrid.electricalInput[2]) annotation(
    Line(points = {{-503, -114}, {-499, -114}, {-499, -102}, {-494, -102}}, color = {255, 200, 0}));
  connect(fileToTransitionOutput4.fileOutput[1], windPowerPlant.fileInput) annotation(
    Line(points = {{-641, -90}, {-628, -90}}, color = {150, 150, 150}));
  connect(distributionPowerGrid1.electricalOutput[1], electricityConsumer.electricalInput) annotation(
    Line(points = {{-426.417, -189.667}, {-397.417, -189.667}}, color = {255, 200, 0}));
  connect(distributionPowerGrid2.electricalOutput[1], conditionalConnection.electricalInput) annotation(
    Line(points = {{-534.167, -130.667}, {-528.167, -130.667}, {-528.167, -113.667}, {-524.167, -113.667}}, color = {255, 200, 0}));
  connect(transformer1.electricalOutput, distributionPowerGrid2.electricalInput[1]) annotation(
    Line(points = {{-587.4, -130}, {-581.4, -130}}, color = {255, 200, 0}));
  connect(logicalExpression7.logicalOutput[1], logicalAnd2.logicalInput2) annotation(
    Line(points = {{-627, -258}, {-613, -258}, {-613, -277}}, color = {53, 28, 117}));
  connect(logicalAnd4.logicalOutput, testController.logicalInput1) annotation(
    Line(points = {{-584, -41}, {-584, -51}, {-570, -51}, {-570, -59}}, color = {53, 28, 117}));
  connect(distributionPowerGrid.electricalOutput[2], battery.electricalInput) annotation(
    Line(points = {{-440.417, -101.667}, {-434.584, -101.667}, {-434.584, -73.9997}, {-455, -73.9997}}, color = {255, 200, 0}));
  annotation(
    Diagram(coordinateSystem(extent = {{-660, 80}, {-380, -340}})));
end Microgrids;
