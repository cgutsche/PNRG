within PNRG.Examples;

model EnergyPark
  PNRG.PowerToX.Electrolyser electrolyser annotation(
    Placement(visible = true, transformation(origin = {-44, 74}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  PNRG.Storage.WaterTank waterTank(maxInputMassFlow = 9.1, maxOutputMassFlow = 9.1, startFilling = 100)  annotation(
    Placement(visible = true, transformation(origin = {30, 44}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Storage.H2Tank h2Tank(maxInputMassFlow = 1.1, maxOutputMassFlow = 1.1)  annotation(
    Placement(visible = true, transformation(origin = {30, 84}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Storage.O2Tank o2Tank(maxInputMassFlow = 8, maxOutputMassFlow = 8)  annotation(
    Placement(visible = true, transformation(origin = {30, 64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput(NOut = 2, fileName = "P:/Programs/PNRG/data.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-160, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput1(NOut = 1, fileName = "P:/Programs/PNRG/data2.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {-160, 104}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.PowerPlants.WindPowerPlant windPowerPlant(number = 5) annotation(
    Placement(visible = true, transformation(origin = {-120, 104}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.PowerPlants.PVPowerPlant pVPowerPlant(areaPV = 1000) annotation(
    Placement(visible = true, transformation(origin = {-120, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.EnergyConsumer.ElectricityConsumer electricityConsumer annotation(
    Placement(visible = true, transformation(origin = {182, 116}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Distribution.OxygenPipe oxygenPipe(NIn = 1, NOut = 1, prioOut = {1})  annotation(
    Placement(visible = true, transformation(origin = {-4, 56}, extent = {{-12, 4}, {12, 12}}, rotation = 0)));
  PNRG.Distribution.HydrogenPipe hydrogenPipe(NIn = 1, NOut = 1, prioOut = {1})  annotation(
    Placement(visible = true, transformation(origin = {-4, 76}, extent = {{-12, 4}, {12, 12}}, rotation = 0)));
  PNRG.Distribution.WaterPipe waterPipe(NIn = 1, NOut = 1, prioOut = {1})  annotation(
    Placement(visible = true, transformation(origin = {-4, 36}, extent = {{12, 4}, {-12, 12}}, rotation = 0)));
  PNRG.Distribution.DistributionPowerGrid distributionPowerGrid(NIn = 5, NOut = 3, prioOut = {1, 2, 3})  annotation(
    Placement(visible = true, transformation(origin = {24, 104}, extent = {{-18, 6}, {18, 18}}, rotation = 0)));
  PNRG.Distribution.OxygenPipe oxygenPipe1(NIn = 1, NOut = 1, prioOut = {1})  annotation(
    Placement(visible = true, transformation(origin = {62, 56}, extent = {{-12, 4}, {12, 12}}, rotation = 0)));
  PNRG.Distribution.HydrogenPipe hydrogenPipe1(NIn = 1, NOut = 2, prioOut = {1, 2})  annotation(
    Placement(visible = true, transformation(origin = {62, 76}, extent = {{-12, 4}, {12, 12}}, rotation = 0)));
  PNRG.Distribution.WaterPipe waterPipe1(NIn = 1, NOut = 1, prioOut = {1})  annotation(
    Placement(visible = true, transformation(origin = {62, 36}, extent = {{12, 4}, {-12, 12}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput11(NOut = 1, fileName = "P:/Programs/PNRG/data4.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {182, 136}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression(NOut = 1, expression = time > 70000)  annotation(
    Placement(visible = true, transformation(origin = {96, 188}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression1(NOut = 1, expression = time > 70000)  annotation(
    Placement(visible = true, transformation(origin = {72, 148}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression11(NOut = 1, expression = time > 70000)  annotation(
    Placement(visible = true, transformation(origin = {72, 174}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression111(NOut = 1, expression = time > 70000)  annotation(
    Placement(visible = true, transformation(origin = {72, 200}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression1111(NOut = 1, expression = true)  annotation(
    Placement(visible = true, transformation(origin = {-28, 202}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression11111(NOut = 1, expression = true)  annotation(
    Placement(visible = true, transformation(origin = {-28, 176}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression111111(NOut = 1, expression = true)  annotation(
    Placement(visible = true, transformation(origin = {-28, 150}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression1111111(NOut = 1, expression = true)  annotation(
    Placement(visible = true, transformation(origin = {-52, 164}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Sources.StochasticSource stochasticSource(NOut = 1) annotation(
    Placement(visible = true, transformation(origin = {182, 38}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.EnergyConsumer.HydrogenConsumer hydrogenConsumer annotation(
    Placement(visible = true, transformation(origin = {182, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Storage.Battery battery(power = 2)  annotation(
    Placement(visible = true, transformation(origin = {20, 138}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression2(NOut = 1, expression =   time> 50000) annotation(
    Placement(visible = true, transformation(origin = {96, 162}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression21(NOut = 1, expression = false) annotation(
    Placement(visible = true, transformation(origin = {-52, 190}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.PowerPlants.NaturalGasPowerPlant naturalGasPowerPlant2 annotation(
    Placement(visible = true, transformation(origin = {-98, -42}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression3(NOut = 1, expression = false) annotation(
    Placement(visible = true, transformation(origin = {-32, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression5(NOut = 1, expression = true) annotation(
    Placement(visible = true, transformation(origin = {-32, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Storage.CO2Storage cO2Storage2(maxInputMassFlow = 220, maxOutputMassFlow = 0) annotation(
    Placement(visible = true, transformation(origin = {-32, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Sources.ConstantSource constantSource2(NOut = 1, out = 1) annotation(
    Placement(visible = true, transformation(origin = {-150, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.Distribution.CO2Pipe cO2Pipe2(NIn = 2, NOut = 1, prioOut = {1}) annotation(
    Placement(visible = true, transformation(origin = {-64, -54}, extent = {{-12, 4}, {12, 12}}, rotation = 0)));
  PNRG.Logics.LogicalExpression logicalExpression4(NOut = 1, expression = true) annotation(
    Placement(visible = true, transformation(origin = {-98, -20}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.Distribution.NaturalGasPipe naturalGasPipe12(NIn = 1, NOut = 1, prioOut = {1}) annotation(
    Placement(visible = true, transformation(origin = {-124, -54}, extent = {{-12, 4}, {12, 12}}, rotation = 0)));
  PNRG.PowerPlants.HydrogenCHPPlant hydrogenCHPPlant annotation(
    Placement(visible = true, transformation(origin = {98, 78}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  PNRG.Distribution.HeatPipe heatPipe(NIn = 2, NOut = 1, prioOut = {1}) annotation(
    Placement(visible = true, transformation(origin = {148, 58}, extent = {{-12, 4}, {12, 12}}, rotation = 0)));
  PNRG.Sources.FileToTransitionOutput fileToTransitionOutput111(NOut = 1, fileName = "P:/Programs/PNRG/data3.txt", tableName = "tab1") annotation(
    Placement(visible = true, transformation(origin = {182, 86}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  PNRG.EnergyConsumer.HeatConsumer heatConsumer annotation(
    Placement(visible = true, transformation(origin = {182, 66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PNRG.PowerPlants.STEPowerPlant sTEPowerPlant annotation(
    Placement(visible = true, transformation(origin = {-120, 24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(fileToTransitionOutput.fileOutput[1], pVPowerPlant.fileInput) annotation(
    Line(points = {{-149, 74}, {-131, 74}}));
  connect(distributionPowerGrid.electricalOutput[2], electrolyser.EnergyIn) annotation(
    Line(points = {{40.5, 116}, {40, 116}, {40, 104}, {-70, 104}, {-70, 74}, {-62, 74}}, color = {255, 200, 0}));
  connect(electrolyser.H2Out, hydrogenPipe.hydrogenInput[1]) annotation(
    Line(points = {{-26, 80}, {-20.9, 80}, {-20.9, 84.4}, {-15.4, 84.4}}, color = {106, 168, 79}));
  connect(hydrogenPipe.hydrogenOutput[1], h2Tank.hydrogenInput) annotation(
    Line(points = {{7, 84}, {19, 84}}, color = {106, 168, 79}));
  connect(oxygenPipe.oxygenOutput[1], o2Tank.oxygenInput) annotation(
    Line(points = {{7, 64}, {19, 64}}, color = {11, 83, 148}));
  connect(o2Tank.oxygenOutput, oxygenPipe1.oxygenInput[1]) annotation(
    Line(points = {{41, 64}, {51, 64}}, color = {11, 83, 148}));
  connect(electrolyser.O2Out, oxygenPipe.oxygenInput[1]) annotation(
    Line(points = {{-26, 68}, {-20.4, 68}, {-20.4, 63.6}, {-15.4, 63.6}}, color = {11, 83, 148}));
  connect(waterPipe1.waterOutput[1], waterTank.waterInput) annotation(
    Line(points = {{51, 44}, {41, 44}}, color = {61, 133, 198}));
  connect(waterTank.waterOutput, waterPipe.waterInput[1]) annotation(
    Line(points = {{19, 44}, {7, 44}}, color = {61, 133, 198}));
  connect(waterPipe.waterOutput[1], electrolyser.WaterIn) annotation(
    Line(points = {{-15, 44}, {-68, 44}, {-68, 65}, {-62, 65}}, color = {61, 133, 198}));
  connect(h2Tank.hydrogenOutput, hydrogenPipe1.hydrogenInput[1]) annotation(
    Line(points = {{41, 84}, {51, 84}}, color = {106, 168, 79}));
  connect(fileToTransitionOutput11.fileOutput[1], electricityConsumer.fileInput) annotation(
    Line(points = {{171, 136}, {166, 136}, {166, 122}, {171, 122}}, color = {150, 150, 150}));
  connect(logicalExpression111111.logicalOutput[1], h2Tank.logicalInput) annotation(
    Line(points = {{-17, 150}, {2, 150}, {2, 90}, {19, 90}}, color = {53, 28, 117}));
  connect(logicalExpression11111.logicalOutput[1], o2Tank.logicalInput) annotation(
    Line(points = {{-17, 176}, {2, 176}, {2, 70}, {19, 70}}, color = {53, 28, 117}));
  connect(logicalExpression1111.logicalOutput[1], waterTank.logicalInput1) annotation(
    Line(points = {{-17, 202}, {2, 202}, {2, 50}, {19, 50}}, color = {53, 28, 117}));
  connect(logicalExpression111.logicalOutput[1], h2Tank.logicalInput1) annotation(
    Line(points = {{61, 200}, {46, 200}, {46, 90}, {41, 90}}, color = {53, 28, 117}));
  connect(logicalExpression11.logicalOutput[1], o2Tank.logicalInput1) annotation(
    Line(points = {{61, 174}, {46, 174}, {46, 70}, {41, 70}}, color = {53, 28, 117}));
  connect(logicalExpression1.logicalOutput[1], waterTank.logicalInput) annotation(
    Line(points = {{61, 148}, {46, 148}, {46, 50}, {41, 50}}, color = {53, 28, 117}));
  connect(logicalExpression1111111.logicalOutput[1], electrolyser.activation) annotation(
    Line(points = {{-63, 164}, {-66, 164}, {-66, 86.5}, {-62, 86.5}, {-62, 83}}, color = {53, 28, 117}));
  connect(fileToTransitionOutput1.fileOutput[1], windPowerPlant.fileInput) annotation(
    Line(points = {{-149, 104}, {-131, 104}}, color = {150, 150, 150}));
  connect(windPowerPlant.electricalOutput, distributionPowerGrid.electricalInput[1]) annotation(
    Line(points = {{-109, 104}, {-81, 104}, {-81, 116}, {7.5, 116}}, color = {255, 200, 0}));
  connect(pVPowerPlant.electricalOutput, distributionPowerGrid.electricalInput[2]) annotation(
    Line(points = {{-109, 74}, {-81, 74}, {-81, 116}, {7.5, 116}}, color = {255, 200, 0}));
  connect(hydrogenPipe1.hydrogenOutput[2], hydrogenConsumer.hydrogenInput) annotation(
    Line(points = {{73, 84}, {75, 84}, {75, 18}, {170, 18}}, color = {106, 168, 79}));
  connect(stochasticSource.fileOutput[1], hydrogenConsumer.fileInput) annotation(
    Line(points = {{171, 38}, {165, 38}, {165, 24}, {171, 24}}, color = {150, 150, 150}));
  connect(constantSource2.fileOutput[1], naturalGasPipe12.naturalGasInput[1]) annotation(
    Line(points = {{-139, -46}, {-135, -46}}, color = {150, 150, 150}, thickness = 0.5));
  connect(naturalGasPipe12.naturalGasOutput[1], naturalGasPowerPlant2.naturalGasInput) annotation(
    Line(points = {{-113, -46}, {-111, -46}, {-111, -47}, {-109, -47}}, color = {126, 58, 0}));
  connect(naturalGasPowerPlant2.cO2Output, cO2Pipe2.co2Input[1]) annotation(
    Line(points = {{-87, -45.2}, {-81, -45.2}, {-81, -46.2}, {-75, -46.2}}));
  connect(cO2Pipe2.co2Output[1], cO2Storage2.co2Input) annotation(
    Line(points = {{-53, -46}, {-43, -46}}, color = {54, 54, 54}));
  connect(logicalExpression4.logicalOutput[1], naturalGasPowerPlant2.activation) annotation(
    Line(points = {{-109, -20}, {-113, -20}, {-113, -36}, {-109, -36}}, color = {53, 28, 117}));
  connect(logicalExpression3.logicalOutput[1], cO2Storage2.logicalInput1) annotation(
    Line(points = {{-21, -24}, {-11, -24}, {-11, -40}, {-21, -40}}, color = {53, 28, 117}));
  connect(logicalExpression5.logicalOutput[1], cO2Storage2.logicalInput) annotation(
    Line(points = {{-43, 0}, {-51, 0}, {-51, -40}, {-43, -40}}, color = {53, 28, 117}));
  connect(cO2Storage2.co2Output, cO2Pipe2.co2Input[2]) annotation(
    Line(points = {{-21, -46}, {-11, -46}, {-11, -58}, {-79, -58}, {-79, -46}, {-75, -46}}, color = {54, 54, 54}));
  connect(distributionPowerGrid.electricalOutput[3], battery.electricalInput) annotation(
    Line(points = {{40.5, 116}, {34, 116}, {34, 138}, {31.5, 138}}, color = {255, 200, 0}));
  connect(battery.electricalOutput, distributionPowerGrid.electricalInput[5]) annotation(
    Line(points = {{9, 138}, {6.25, 138}, {6.25, 116}, {7.5, 116}}, color = {255, 200, 0}));
  connect(logicalExpression21.logicalOutput[1], battery.logicalInput1) annotation(
    Line(points = {{-41, 190}, {2, 190}, {2, 144}, {9, 144}}, color = {53, 28, 117}));
  connect(logicalExpression2.logicalOutput[1], battery.logicalInput) annotation(
    Line(points = {{85, 162}, {46, 162}, {46, 144}, {31, 144}}, color = {53, 28, 117}));
  connect(hydrogenCHPPlant.electricalOutput, distributionPowerGrid.electricalInput[3]) annotation(
    Line(points = {{115.6, 87.6}, {131.6, 87.6}, {131.6, 125.6}, {-0.4, 125.6}, {-0.4, 116}, {7.5, 116}}, color = {255, 200, 0}));
  connect(logicalExpression.logicalOutput[1], hydrogenCHPPlant.activation) annotation(
    Line(points = {{85, 188}, {46, 188}, {46, 88}, {80, 88}}, color = {53, 28, 117}));
  connect(oxygenPipe1.oxygenOutput[1], hydrogenCHPPlant.O2In) annotation(
    Line(points = {{73, 64}, {76.5, 64}, {76.5, 68}, {80, 68}}, color = {11, 83, 148}));
  connect(hydrogenPipe1.hydrogenOutput[1], hydrogenCHPPlant.H2In) annotation(
    Line(points = {{73, 84}, {75.5, 84}, {75.5, 78}, {80, 78}}, color = {106, 168, 79}));
  connect(hydrogenCHPPlant.WaterOut, waterPipe1.waterInput[1]) annotation(
    Line(points = {{115.6, 68.4}, {119.6, 68.4}, {119.6, 44.4}, {72.6, 44.4}}, color = {61, 133, 198}));
  connect(hydrogenCHPPlant.heatOutput, heatPipe.heatInput[2]) annotation(
    Line(points = {{115.6, 78}, {131.6, 78}, {131.6, 66}, {136.6, 66}}, color = {255, 80, 50}));
  connect(fileToTransitionOutput111.fileOutput[1], heatConsumer.fileInput) annotation(
    Line(points = {{171, 86}, {166, 86}, {166, 72}, {172, 72}}, color = {150, 150, 150}));
  connect(heatPipe.heatOutput[1], heatConsumer.heatInput) annotation(
    Line(points = {{159, 66}, {171, 66}}, color = {255, 80, 50}));
  connect(sTEPowerPlant.heatOutput, heatPipe.heatInput[1]) annotation(
    Line(points = {{-109, 24}, {132, 24}, {132, 66}, {137, 66}}, color = {255, 80, 50}));
  connect(fileToTransitionOutput.fileOutput[2], sTEPowerPlant.fileInput) annotation(
    Line(points = {{-149, 74}, {-141, 74}, {-141, 24}, {-131, 24}}, color = {150, 150, 150}));
  connect(distributionPowerGrid.electricalOutput[1], electricityConsumer.electricalInput) annotation(
    Line(points = {{40.5, 116}, {171, 116}}, color = {255, 200, 0}));
  connect(naturalGasPowerPlant2.electricalOutput, distributionPowerGrid.electricalInput[4]) annotation(
    Line(points = {{-87, -39}, {-87, -38.5}, {-81, -38.5}, {-81, 116}, {7.5, 116}}, color = {255, 200, 0}));
protected
  annotation(
    uses(PNlib(version = "2.2")),
    Diagram(coordinateSystem(extent = {{-180, 220}, {200, -120}})),
    version = "");
end EnergyPark;
